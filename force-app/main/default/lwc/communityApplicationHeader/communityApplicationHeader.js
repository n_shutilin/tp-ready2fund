import { LightningElement, api } from 'lwc';

export default class CommunityApplicationHeader extends LightningElement {
	@api logoURL;
	@api phoneURL;
	@api accountURL;
	@api talkUsURL;

	isShowSelectItems = false;
	isShowAccountMenu = false;
	isShowMobileMenu = false;

	selectClick(event) {
		event.preventDefault();
		this[event.currentTarget.dataset.cklick] = !this[event.currentTarget.dataset.cklick];
		event.currentTarget.querySelector('svg').classList.toggle('open');
		event.stopPropagation();
	}

	pressKey(e) {
		if (e.keyCode === 13 || e.keyCode === 32) {
			this.selectClick(e);
		}
	}

	blurSeect(e) {
		this.isShowSelectItems = false;
	}

	toggleMenu(e) {
		e.currentTarget.classList.toggle('open');
		this.isShowMobileMenu = !this.isShowMobileMenu;
	}
}