import { LightningElement, api, track } from 'lwc';

const defaultSlider = {
    'refinance': false,
    'mortgage': false,
    'homeOne': false,
    'homeTwo': false,
    'loan': false,
    'income': false,
    'goverment': false,
    'credit': false,
    'solution': false,
    'files': false,
};

const parts = [
    { 'refinance': true, },
    { 'mortgage': true, },
    { 'homeOne': true, },
    { 'homeTwo': true, },
    { 'loan': true, },
    { 'income': true, },
    { 'goverment': true, },
    { 'credit': true, },
    { 'solution': true, },
    { 'files': true, },
]

export default class CommunityApplicationQuestions extends LightningElement {

    @api applicationId;
    @api maxIndexPage;

    @track slider = defaultSlider;

    @api selectSlide(index) {
        this.slider = {...defaultSlider, ...parts[index]};
    }

}