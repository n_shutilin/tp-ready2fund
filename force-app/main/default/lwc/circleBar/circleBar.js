import { LightningElement, api, track } from 'lwc';

const SLIDE = {
	0: 0,
	1: 15,
	2: 25,
	3: 35,
	4: 45,
	5: 50,
	6: 55,
	7: 60,
	8: 70,
	9: 80,
};

export default class CircleBar extends LightningElement {
	text = 'Start';
	isShowArrow = false;
	@track currentSlide = 'c-ProgressPie__value';

	@api changeSlide(slideNumber) {
		this.text = SLIDE[slideNumber] + '%';
		this.currentSlide = 'c-ProgressPie__value ' + (slideNumber !== 0 ? 'c-ProgressPie__value--slide' + slideNumber : '');
		this.isShowArrow = this.text === 'Start';
	}

	renderedCallback() {
		this.isShowArrow = this.text === 'Start';
	}
}