import { LightningElement, api } from 'lwc';
import SAVE_CURRENT_MORTGAGE from '@salesforce/apex/CompleteApplication.saveMyCurrentMortgage';

import { showSpinner, hideSpinner, showToast, showNextSlide } from 'c/communityHelper';
const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationCurrentMortgage extends LightningElement {

    //applicationId = 'a005Y00002IMzApQAL';
    //borrowerId = 'a065Y00001uVFwMQAW';

    @api applicationId = 'a005Y00002IMzApQAL';    
    mortgagePayment = 0;
    pmtIncludesPI = false;
    pmtIncludesPILabel = 'Does the payment entered include property taxes and/or homeowners insurance?';
    mortgageLien = 0;
    marketValue = 0;
    survivingSpouse = false;
    survivingSpouseLabel = 'Are you military veteran, active-duty service member or surviving spouse?';

    connectedCallback() {
        hideSpinner(this);
    }


    setCurrencyField(event) {            
        let val = event.currentTarget.value.split('.').join('');        
        if (checkNumbers.test(val) || val === '') {
            this[event.currentTarget.dataset.name] = val;
            event.currentTarget.value = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    }

    setAnswers(event) {
        this[event.detail.name] = event.detail.value;
    }

    saveApplication() {
        const mortgageWrapper = {};
        mortgageWrapper.applicationId = this.applicationId;
        mortgageWrapper.borrowerId = this.borrowerId;
        mortgageWrapper.mortgagePayment = this.mortgagePayment;
        mortgageWrapper.pmtIncludesPI = this.pmtIncludesPI;
        mortgageWrapper.mortgageLien = this.mortgageLien;
        mortgageWrapper.marketValue = this.marketValue;
        mortgageWrapper.militarySurvivingSpouse = this.survivingSpouse;

        console.log(mortgageWrapper);

        SAVE_CURRENT_MORTGAGE({ wrapper: mortgageWrapper })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log(err);
            });
    }
}