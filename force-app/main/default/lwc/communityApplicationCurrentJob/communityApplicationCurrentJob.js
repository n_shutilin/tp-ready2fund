import { LightningElement, api, track } from 'lwc';

const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationCurrentJob extends LightningElement {

    @api index;
    @api jobInfo;
    @track jobTypes = [
        { name: 'Full-time', value: 'Full-time', isSelected: false },
        { name: 'Part-time', value: 'Part-time', isSelected: false },
        { name: 'Seasonal', value: 'Seasonal', isSelected: false },
    ];

    isEmployerFamilyMember = '';
    calculatedBonusClasses = 'customCheckboxContainer'
    calculatedCommissionClasses = 'customCheckboxContainer'
    calculatedOvertimeClasses = 'customCheckboxContainer'

    renderedCallback() {
        console.log(JSON.parse(JSON.stringify(this.jobInfo)));
        this.jobTypes.forEach(item => {
            if (item.value === this.jobInfo.jobType) {
                item.isSelected = true;
            } else {
                item.isSelected = false;
            }
        });

        this.isEmployerFamilyMember = this.jobInfo.isEmployerFamilyMember ? 'Yes' : 'No';
        this.calculatedBonusClasses = this.jobInfo.bonus.isSelected ? 'customCheckboxContainer checked' : 'customCheckboxContainer';
        this.calculatedCommissionClasses = this.jobInfo.commission.isSelected ? 'customCheckboxContainer checked' : 'customCheckboxContainer';
        this.calculatedOvertimeClasses = this.jobInfo.overtime.isSelected ? 'customCheckboxContainer checked' : 'customCheckboxContainer';

        this.template.querySelectorAll('.currencyField').forEach(item => {
            item.value = item.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        });
    }

    setTextField(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
    }

    typePhoneNumber(event) {
        event.preventDefault();
        if (checkNumbers.test(event.currentTarget.value) || event.currentTarget.value === '') {
            this[event.currentTarget.dataset.name] = event.currentTarget.value;
            this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
            if (event.currentTarget.dataset.name !== 'three' && event.currentTarget.value.length === 3) {
                event.target.nextSibling.nextSibling.focus();
            }
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name];
            return;
        }
    }

    toggleBorder(event) {
        event.currentTarget.parentNode.classList.toggle('active');
    }

    selectJobType(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
    }

    setDateValue(event) {
        let val = event.currentTarget.value;
        if (checkNumbers.test(val) || val === '') {
            if (val.length <= 4 && event.currentTarget.dataset.name === 'startYear') {
                this[event.currentTarget.dataset.name] = val;
                this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
            } else if ((val.length <= 2 && event.currentTarget.dataset.name == 'startMonth' && +val > 0 && +val < 13) || val === '') {
                this[event.currentTarget.dataset.name] = val;
                this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
            } else {
                event.currentTarget.value = this[event.currentTarget.dataset.name];
            }
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name];
        }
    }

    setCurrencyField(event) {
        event.preventDefault();
        event.stopPropagation();
        let val = event.currentTarget.value.split('.').join('');
        if (checkNumbers.test(val) || val === '') {
            this[event.currentTarget.dataset.name] = val;
            this.setValue(this.index, event.currentTarget.dataset.name, val);
            event.currentTarget.value = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    }

    setIncomeType(event) {
        const key = event.currentTarget.dataset.name;
        const value = event.currentTarget.dataset.atr === 'isSelected'
            ? !(event.currentTarget.dataset.value === 'true')
            : event.currentTarget.value;

        this.setValue(this.index, event.currentTarget.dataset.name, Object.assign(this.createAdditionalIncomeObj(event.currentTarget.dataset.name), { [event.currentTarget.dataset.atr]: value, }));
    }

    updateValues(event) {
        this.setValue(this.index, event.detail.name, Object.assign(this.createAdditionalIncomeObj(event.detail.name), { [event.detail.atr]: event.detail.value, }));
    }

    setFamilyMember(event) {
        this.setValue(this.index, event.detail.name, event.detail.value);
    }

    setValue(index, name, value) {
        this.dispatchEvent(new CustomEvent('currentjob', {
            detail: { index, data: { [name]: value } },
        }));
    }

    cancel() {
        this.dispatchEvent(new CustomEvent('cancelcurrentjob', {
            detail: { index: this.index },
        }));
    }

    switchToEdit() {
        this.dispatchEvent(new CustomEvent('editcurrentjob', {
            detail: { index: this.index, target: 'currentJobs' },
        }));
    }

    createAdditionalIncomeObj(key) {
        return {
            isSelected: this.jobInfo[key].isSelected,
            primaryYear: this.jobInfo[key].primaryYear,
            primaryAmount: this.jobInfo[key].primaryAmount,
            secondaryYear: this.jobInfo[key].secondaryYear,
            secondaryAmount: this.jobInfo[key].secondaryAmount,
        }
    }
}