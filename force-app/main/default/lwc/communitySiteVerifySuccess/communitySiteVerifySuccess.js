import { LightningElement } from 'lwc';
import EMAIL_PICTURES from '@salesforce/resourceUrl/emailPictures';

export default class CommunitySiteVerifySuccess extends LightningElement {
	imgURL = `${EMAIL_PICTURES}/emailPictures/emailOk.jpg`;

	goNext() {
		this.dispatchEvent(new CustomEvent('gonext'));
	}
}