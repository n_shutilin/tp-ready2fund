import { LightningElement, track } from 'lwc';
import SAVE_HOME_INFO_FIRST_PART from '@salesforce/apex/CompleteApplication.saveHomeInfoFirstPart';

const phoneRegex = new RegExp(/^\d+$/);

export default class CommunityApplicationHomeInfoOne extends LightningElement {

    selectedState = '';
    one = '';
    two = '';
    three = '';
    propertyAddress = '';
    address = '';
    city = '';
    postalCode = '';
    isShowPrompt = false;
    applicationId = 'a005Y00002IMzApQAL';
    reoId = 'a0n5Y00001wJnaPQAS';


    @track states = [
        { name: 'Alabama', value: 'AL', isSelected: false },
        { name: 'Alaska', value: 'AK', isSelected: false },
        { name: 'Arizona', value: 'AZ', isSelected: false },
        { name: 'Arkansas', value: 'AR', isSelected: false },
        { name: 'California', value: 'CA', isSelected: false },
        { name: 'Colorado', value: 'CO', isSelected: false },
        { name: 'Connecticut', value: 'CT', isSelected: false },
        { name: 'Delaware', value: 'DE', isSelected: false },
        { name: 'District of Columbia', value: 'DC', isSelected: false },
        { name: 'Florida', value: 'FL', isSelected: false },
        { name: 'Georgia', value: 'GA', isSelected: false },
        { name: 'Hawaii', value: 'HI', isSelected: false },
        { name: 'Idaho', value: 'ID', isSelected: false },
        { name: 'Illinois', value: 'IL', isSelected: false },
        { name: 'Indiana', value: 'IN', isSelected: false },
        { name: 'Iowa', value: 'IA', isSelected: false },
        { name: 'Kansas', value: 'KS', isSelected: false },
        { name: 'Kentucky', value: 'KY', isSelected: false },
        { name: 'Louisiana', value: 'LA', isSelected: false },
        { name: 'Maine', value: 'ME', isSelected: false },
        { name: 'Maryland', value: 'MD', isSelected: false },
        { name: 'Massachusetts', value: 'MA', isSelected: false },
        { name: 'Michigan', value: 'MI', isSelected: false },
        { name: 'Minnesota', value: 'MN', isSelected: false },
        { name: 'Mississippi', value: 'MS', isSelected: false },
        { name: 'Missouri', value: 'MO', isSelected: false },
        { name: 'Montana', value: 'MT', isSelected: false },
        { name: 'Nebraska', value: 'NE', isSelected: false },
        { name: 'Nevada', value: 'NV', isSelected: false },
        { name: 'New Hampshire', value: 'NH', isSelected: false },
        { name: 'New Jersey', value: 'NJ', isSelected: false },
        { name: 'New Mexico', value: 'NM', isSelected: false },
        { name: 'New York', value: 'NY', isSelected: false },
        { name: 'North Carolina', value: 'NC', isSelected: false },
        { name: 'North Dakota', value: 'ND', isSelected: false },
        { name: 'Ohio', value: 'OH', isSelected: false },
        { name: 'Oklahoma', value: 'OK', isSelected: false },
        { name: 'Oregon', value: 'OR', isSelected: false },
        { name: 'Pennsylvania', value: 'PA', isSelected: false },
        { name: 'Puerto Rico', value: 'PR', isSelected: false },
        { name: 'Rhode Island', value: 'RI', isSelected: false },
        { name: 'South Carolina', value: 'SC', isSelected: false },
        { name: 'South Dakota', value: 'SD', isSelected: false },
        { name: 'Tennessee', value: 'TN', isSelected: false },
        { name: 'Texas', value: 'TX', isSelected: false },
        { name: 'Utah', value: 'UT', isSelected: false },
        { name: 'Vermont', value: 'VT', isSelected: false },
        { name: 'Virgin Islands', value: 'VI', isSelected: false },
        { name: 'Virginia', value: 'VA', isSelected: false },
        { name: 'Washington', value: 'WA', isSelected: false },
        { name: 'West Virginia', value: 'WV', isSelected: false },
        { name: 'Wisconsin', value: 'WI', isSelected: false },
        { name: 'Wyoming', value: 'WY', isSelected: false },
    ];

    selectState(event) {        
        this.states.forEach(item => {
            console.log('item ', item);
            if (item.value === event.currentTarget.value) {
                item.isSelected = true;
                this.selectedState = item.value;
            } else {
                item.isSelected = false;
            }
        });
    }

    typePhoneNumber(event) {       
        event.preventDefault();
        if (phoneRegex.test(event.currentTarget.value) || event.currentTarget.value === '') {
            this[event.currentTarget.dataset.part] = event.currentTarget.value;   
            console.log('event.currentTarget.dataset.part ', event.currentTarget.dataset.part !== 'three');       
            console.log('event.currentTarget.value.length === 3 ', event.currentTarget.value.length === 3);       
            if (event.currentTarget.dataset.part !== 'three' && event.currentTarget.value.length === 3) {
                console.log(event.target.nextSibling.nextSibling);
                event.target.nextSibling.nextSibling.focus();
            } 
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.part];
            return;
        }
    }

    toggleBorder(event) {
        event.currentTarget.parentNode.classList.toggle('active');
    }

    togglePrompt() {
        this.isShowPrompt = !this.isShowPrompt;
    }

    setPropertyAddress(event) {
        this.propertyAddress = event.currentTarget.value;
    }

    setAddressLine(event) {
        this.address = event.currentTarget.value;
    }

    setCity(event) {
        this.city = event.currentTarget.value;
    }

    setZipCode(event) {
        this.postalCode = event.currentTarget.value;
    }

    saveHomeInfo() {
        const firstHomeInfo = {};
        firstHomeInfo.applicationId = this.applicationId;
        firstHomeInfo.reoId = this.reoId;
        firstHomeInfo.address = this.propertyAddress;
        firstHomeInfo.address2 = this.address;
        firstHomeInfo.city = this.city;
        firstHomeInfo.state = this.selectedState;
        firstHomeInfo.postalCode = this.postalCode;

        console.log(firstHomeInfo);

        SAVE_HOME_INFO_FIRST_PART({ wrapper: firstHomeInfo })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log(err);
            });
    }
}