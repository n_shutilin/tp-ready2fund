import { LightningElement, api } from 'lwc';

export default class CommunityApplicationFooter extends LightningElement {
	@api logoURL;

	openMenu(event) {
		event.preventDefault();
		event.currentTarget.parentElement.querySelector('ul').classList.toggle('openMenu');
	}
}