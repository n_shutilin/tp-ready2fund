import { LightningElement, api, track } from 'lwc';

const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationAdditionalIncomeInfo extends LightningElement {
    @api name;
    @api atr;
    @api selectedYear;
    @api amount;
    @track years = this.getYearPicklist();

    renderedCallback() {
        this.years.forEach(item => {
            console.log('item.value ', item.value);
            console.log('+this.selectedYear ', +this.selectedYear);
            if (item.value === +this.selectedYear) {
                item.isSelected = true;
            } else {
                item.isSelected = false;
            }
        });
        this.template.querySelectorAll('.currencyField').forEach(item => {
            item.value = item.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        });
    }

    selectYear(event) {
        this.sendEvent(this.atr + 'Year', event.currentTarget.value);
    }

    setCurrencyField(event) {  
        console.log('work');  
        let val = event.currentTarget.value.split('.').join('');        
        if (checkNumbers.test(val) || val === '') {
            this.sendEvent(this.atr + 'Amount', val);
        } else {
            event.currentTarget.value = this.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    }

    getYearPicklist() {
        const result = [];
        const currentYear = new Date().getFullYear();
        for(let i = 0; i < 3; i++) {
            result.push({ name: currentYear - i, value: currentYear - i, isSelected: false });
        }
        return result;
    }

    sendEvent(atr, value) {
        this.dispatchEvent(new CustomEvent('changecomponent', { detail: { name: this.name, atr, value } }));
    }
}