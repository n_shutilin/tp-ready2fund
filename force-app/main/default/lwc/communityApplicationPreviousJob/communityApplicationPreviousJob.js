import { LightningElement, api } from 'lwc';

const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationPreviousJob extends LightningElement {

    @api index;
    @api jobInfo;
    startYear = '';
    endYear = '';
    startMonth = '';
    endMonth = '';

    setTextField(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
    }

    setDateValue(event) {            
        let val = event.currentTarget.value;        
        if (checkNumbers.test(val) || val === '') {
            if (val.length <= 4 && (event.currentTarget.dataset.name === 'startYear' || event.currentTarget.dataset.name === 'endYear')) {
                this[event.currentTarget.dataset.name] = val;
                this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
            } else if ( (val.length <= 2 && ( event.currentTarget.dataset.name == 'startMonth' || event.currentTarget.dataset.name == 'endMonth') && +val >0 && +val < 13) || val === '') {
                this[event.currentTarget.dataset.name] = val;
                this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
            } else {
                event.currentTarget.value = this[event.currentTarget.dataset.name];
            }
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name];
        }
    }    

    setValue(index, name, value) {
        this.dispatchEvent(new CustomEvent('previousjob', {
            detail: { index, data: { [name] : value } },
        }));
    }

    cancel() {
        this.dispatchEvent(new CustomEvent('cancelpreviousjob', {
            detail: { index: this.index },
        }));
    }

    switchToEdit() {
        this.dispatchEvent(new CustomEvent('editpreviousjob', {
            detail: { index: this.index, target: 'previousJobs' },
        }));
    }

}