import { LightningElement } from 'lwc';
import SAVE_CREDIT from '@salesforce/apex/CompleteApplication.saveMyCredit';

const numberRegex = new RegExp(/^\d+$/);

export default class CommunityApplicationCreditQuestion extends LightningElement {

    applicationId = 'a005Y00002IMzApQAL';
    borrowerId = 'a065Y00001uVFwMQAW';

    month = '';
    day = '';
    year = '';

    ssn = '';

    one = '';
    two = '';
    three = '';

    toggleMenu(event) {
        event.preventDefault();
        event.currentTarget.querySelector('svg').classList.toggle('open');
        event.currentTarget.parentNode.classList.toggle('open');
    }

    typeDate(event) {
        event.preventDefault();
        if (numberRegex.test(event.currentTarget.value) || event.currentTarget.value === '') {   
            if (event.currentTarget.dataset.part !== 'year' && event.currentTarget.value.length === 2) {
                if (event.currentTarget.dataset.part === 'month' && (+event.currentTarget.value < 1 || +event.currentTarget.value > 12)) {
                    event.currentTarget.value = this[event.currentTarget.dataset.part];
                    return;
                } else if (event.currentTarget.dataset.part === 'day' && (+event.currentTarget.value < 1 || +event.currentTarget.value > 31)) {
                    event.currentTarget.value = this[event.currentTarget.dataset.part];
                    return;
                }
                event.target.nextSibling.nextSibling.focus();
            }
            this[event.currentTarget.dataset.part] = event.currentTarget.value;
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.part];
            return;
        }
    }

    typeSocial(event) {
        const value = event.currentTarget.value.split('-').join('');
        if (numberRegex.test(value) || value === '') {            
            const temp = value.split('');
            for (let i = 0; i < temp.length; i++) {
                if (i === 3 || i === 6) {
                    temp.splice(i, 0, '-');
                } 
            }
            this[event.currentTarget.dataset.name] = temp.join('');
            event.currentTarget.value = this[event.currentTarget.dataset.name];
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name];
            return;
        }
    }

    typePhoneNumber(event) {
        event.preventDefault();
        if (numberRegex.test(event.currentTarget.value) || event.currentTarget.value === '') {
            this[event.currentTarget.dataset.part] = event.currentTarget.value;
            console.log('event.currentTarget.dataset.part ', event.currentTarget.dataset.part !== 'three');
            console.log('event.currentTarget.value.length === 3 ', event.currentTarget.value.length === 3);
            if (event.currentTarget.dataset.part !== 'three' && event.currentTarget.value.length === 3) {
                console.log(event.target.nextSibling.nextSibling);
                event.target.nextSibling.nextSibling.focus();
            }
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.part];
            return;
        }
    }

    toggleBorder(event) {
        event.currentTarget.parentNode.classList.toggle('active');
    }

    togglePrompt() {
        this.isShowPrompt = !this.isShowPrompt;
    }

    saveCreditInfo() {
        const creditWrapper = {};
        creditWrapper.applicationId = this.applicationId;
        creditWrapper.borrowerId = this.borrowerId;
        creditWrapper.dateOfBirth = this.month + '/' + this.day + '/' + this.year;
        creditWrapper.ssn = this.ssn.split('-').join('');
        creditWrapper.mobile = this.one + this.two + this.three;

        console.log(creditWrapper);

        SAVE_CREDIT({ wrapper: creditWrapper })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log(err);
            });
    }
}