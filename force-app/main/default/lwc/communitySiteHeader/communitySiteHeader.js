import { LightningElement } from 'lwc';
import SITE_IMGS from '@salesforce/resourceUrl/communitySiteIMG';

export default class CommunitySiteHeader extends LightningElement {
	logoURL = SITE_IMGS + '/logo.png';
	phoneURL = SITE_IMGS + '/icon-phone.png';

	signIn() {
		const url = window.location.href;
		const index = url.indexOf('/s');
		window.location.assign(url.slice(0, index) + '/s/login');
	}
}