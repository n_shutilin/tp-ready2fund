import { LightningElement, api, track } from 'lwc';
import ASSETS from '@salesforce/resourceUrl/assets';

import HEADER_SLIDE_ONE from '@salesforce/label/c.headerSlideOne';
import IMG_ONE_SLIDE_ONE from '@salesforce/label/c.imgOneSlideOne';
import IMG_TWO_SLIDE_ONE from '@salesforce/label/c.imgTwoSlideOne';
import IMG_THREE_SLIDE_ONE from '@salesforce/label/c.imgThreeSlideOne';

import HEADER_SLIDE_TWO from '@salesforce/label/c.headerSlideTwo';
import IMG_ONE_SLIDE_TWO from '@salesforce/label/c.imgOneSlideTwo';
import IMG_TWO_SLIDE_TWO from '@salesforce/label/c.imgTwoSlideTwo';
import IMG_THREE_SLIDE_TWO from '@salesforce/label/c.imgThreeSlideTwo';
import IMG_FOUR_SLIDE_TWO from '@salesforce/label/c.imgFourSlideTwo';

import HEADER_SLIDE_THREE from '@salesforce/label/c.headerSlideThree';
import IMG_ONE_SLIDE_THREE from '@salesforce/label/c.imgOneSlideThree';
import IMG_TWO_SLIDE_THREE from '@salesforce/label/c.imgTwoSlideThree';
import IMG_THREE_SLIDE_THREE from '@salesforce/label/c.imgThreeSlideThree';

import HEADER_SLIDE_FOUR from '@salesforce/label/c.headerSlideFour';
import OPTION_ONE_SLIDE_FOUR from '@salesforce/label/c.optionOneSlideFour';
import OPTION_TWO_SLIDE_FOUR from '@salesforce/label/c.optionTwoSlideFour';
import OPTION_THREE_SLIDE_FOUR from '@salesforce/label/c.optionThreeSlideFour';
import OPTION_FOUR_SLIDE_FOUR from '@salesforce/label/c.optionFourSlideFour';
import OPTION_FIVE_SLIDE_FOUR from '@salesforce/label/c.optionFiveSlideFour';
import OPTION_SIX_SLIDE_FOUR from '@salesforce/label/c.optionSixSlideFour';
import OPTION_SEVEN_SLIDE_FOUR from '@salesforce/label/c.optionSevenSlideFour';

import HEADER_SLIDE_FIVE from '@salesforce/label/c.headerSlideFive';
import IMG_ONE_SLIDE_FIVE from '@salesforce/label/c.imgOneSlideFive';
import IMG_TWO_SLIDE_FIVE from '@salesforce/label/c.imgTwoSlideFive';

import HEADER_SLIDE_SIX from '@salesforce/label/c.headerSlideSix';
import IMG_ONE_SLIDE_SIX from '@salesforce/label/c.imgOneSlideSix';
import IMG_TWO_SLIDE_SIX from '@salesforce/label/c.imgTwoSlideSix';
import IMG_THREE_SLIDE_SIX from '@salesforce/label/c.imgThreeSlideSix';
import IMG_FOUR_SLIDE_SIX from '@salesforce/label/c.imgFourSlideSix';
import IMG_FIVE_SLIDE_SIX from '@salesforce/label/c.imgFiveSlideSix';

import HEADER_SLIDE_SEVEN from '@salesforce/label/c.headerSlideSeven';

import APEX_SUBMIT from '@salesforce/apex/SubmitApplication.submit';
import APEX_CHECK_EMAIL from '@salesforce/apex/SubmitApplication.checkEmail';
import APEX_SET_PASSWORD from '@salesforce/apex/SubmitApplication.createSiteUser';

const checkFields = new RegExp(/^([a-zA-Z '-]*)$/);
const checkEmail = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
const checkUpper = new RegExp(/[A-Z]/);
const checkLower = new RegExp(/[a-z]/);
const checkNumber = new RegExp(/[0-9]/);

export default class CommunitySiteBody extends LightningElement {
	@api rId;
	@api paId;
	@api headerSlideNine;
	currentSlide = 0;
	isShowSVGSlide = true;
	isShowOptionsSlide = false;
	IsShowFieldsSlide = false;
	isRefinance = false;
	isShowConfirmButton = true;
	isShowButtonBack = false;
	IsShowAgreementSlide = false;
	isShowSpinner = false;
	firstName = true;
	isFirstLoad = false;

	// start setup data
	assets = {
		currentSlide: 0,
		0: {
			header: HEADER_SLIDE_ONE,
			disabled: 'disabled',
			options: [
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_ONE_SLIDE_ONE),
					svgURL: `${ASSETS}/assets/typeOfLoan/homeRefinance.txt#homeRefinance`,
					selected: false,
					value: 'Home Refinance',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_TWO_SLIDE_ONE),
					svgURL: `${ASSETS}/assets/typeOfLoan/homePurchase.txt#homePurchase`,
					selected: false,
					value: 'Home Purchase',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_THREE_SLIDE_ONE),
					svgURL: `${ASSETS}/assets/typeOfLoan/cashOutRefinance.txt#cashOutRefinance`,
					selected: false,
					value: 'Cash-Out Refinance',
				},
			],
		},
		1: {
			header: HEADER_SLIDE_TWO,
			disabled: 'disabled',
			options: [
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_ONE_SLIDE_TWO),
					svgURL: `${ASSETS}/assets/homeDescription/singleFamily.txt#singleFamily`,
					selected: false,
					value: 'Single Family',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_TWO_SLIDE_TWO),
					svgURL: `${ASSETS}/assets/homeDescription/multiFamily.txt#multiFamily`,
					selected: false,
					value: 'Multi-Family',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_THREE_SLIDE_TWO),
					svgURL: `${ASSETS}/assets/homeDescription/condominium.txt#condominium`,
					selected: false,
					value: 'Condominium',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_FOUR_SLIDE_TWO),
					svgURL: `${ASSETS}/assets/homeDescription/townhouse.txt#townhouse`,
					selected: false,
					value: 'Townhouse',
				},
			],
		},
		2: {
			header: HEADER_SLIDE_THREE,
			disabled: 'disabled',
			options: [
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_ONE_SLIDE_THREE),
					svgURL: `${ASSETS}/assets/primaryResidence/primaryResidence.txt#primaryResidence`,
					selected: false,
					value: 'Primary Residence',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_TWO_SLIDE_THREE),
					svgURL: `${ASSETS}/assets/primaryResidence/secondaryHome.txt#secondaryHome`,
					selected: false,
					value: 'Secondary Home',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_THREE_SLIDE_THREE),
					svgURL: `${ASSETS}/assets/primaryResidence/investmentProperty.txt#investmentProperty`,
					selected: false,
					value: 'Investment Property',
				},
			],
		},
		3: {
			header: HEADER_SLIDE_FOUR,
			disabled: 'disabled',
			options: [
				{ key: this.uniqueId(), caption: OPTION_ONE_SLIDE_FOUR, svgURL: '', selected: false, value: 'Immediate' },
				{ key: this.uniqueId(), caption: OPTION_TWO_SLIDE_FOUR, svgURL: '', selected: false, value: 'Found a House/Offer Pending' },
				{ key: this.uniqueId(), caption: OPTION_THREE_SLIDE_FOUR, svgURL: '', selected: false, value: 'Whithin 30 Days' },
				{ key: this.uniqueId(), caption: OPTION_FOUR_SLIDE_FOUR, svgURL: '', selected: false, value: '2-3 Months' },
				{ key: this.uniqueId(), caption: OPTION_FIVE_SLIDE_FOUR, svgURL: '', selected: false, value: '3-6 Months' },
				{ key: this.uniqueId(), caption: OPTION_SIX_SLIDE_FOUR, svgURL: '', selected: false, value: '6+ Months' },
				{ key: this.uniqueId(), caption: OPTION_SEVEN_SLIDE_FOUR, svgURL: '', selected: false, value: 'No Time Frame' },
			],
		},
		4: {
			header: HEADER_SLIDE_FIVE,
			disabled: 'disabled',
			options: [
				{ key: this.uniqueId(), caption: this.parseStr(IMG_ONE_SLIDE_FIVE), svgURL: `${ASSETS}/assets/yesNo/yes.txt#yes`, selected: false, value: 'Yes' },
				{ key: this.uniqueId(), caption: this.parseStr(IMG_TWO_SLIDE_FIVE), svgURL: `${ASSETS}/assets/yesNo/no.txt#no`, selected: false, value: 'No' },
			],
		},
		5: {
			header: HEADER_SLIDE_SIX,
			disabled: 'disabled',
			options: [
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_ONE_SLIDE_SIX),
					svgURL: `${ASSETS}/assets/creditProfile/excellent.txt#excellent`,
					selected: false,
					value: 'Excellent (720+)',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_TWO_SLIDE_SIX),
					svgURL: `${ASSETS}/assets/creditProfile/good.txt#good`,
					selected: false,
					value: 'Good (660-719)',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_THREE_SLIDE_SIX),
					svgURL: `${ASSETS}/assets/creditProfile/avg.txt#avg`,
					selected: false,
					value: 'Avg. (620-659)',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_FOUR_SLIDE_SIX),
					svgURL: `${ASSETS}/assets/creditProfile/belowAvg.txt#belowAvg`,
					selected: false,
					value: 'Below Avg. (580-619)',
				},
				{
					key: this.uniqueId(),
					caption: this.parseStr(IMG_FIVE_SLIDE_SIX),
					svgURL: `${ASSETS}/assets/creditProfile/poor.txt#poor`,
					selected: false,
					value: 'Poor (<579)',
				},
			],
		},
		6: {
			header: HEADER_SLIDE_SEVEN,
			disabled: 'disabled',
			options: [
				{ key: this.uniqueId(), caption: this.parseStr(IMG_ONE_SLIDE_FIVE), svgURL: `${ASSETS}/assets/yesNo/yes.txt#yes`, selected: false, value: 'Yes' },
				{ key: this.uniqueId(), caption: this.parseStr(IMG_TWO_SLIDE_FIVE), svgURL: `${ASSETS}/assets/yesNo/no.txt#no`, selected: false, value: 'No' },
			],
		},
		7: {
			header: '',
			disabled: '',
			options: [
				{
					key: this.uniqueId(),
					caption: 'First Name',
					isEmpty: false,
					emptyErrorMessage: 'Please enter your first name',
					hasError: false,
					errorMessage: 'Please use only letters, apostrophes and hyphens, and limit your entry to between 2 and 50 characters.',
					value: '',
					type: 'text',
					isErrorOnBD: false,
					errorMessageFromBD: '',
				},
				{
					key: this.uniqueId(),
					caption: 'Last Name',
					isEmpty: false,
					emptyErrorMessage: 'Please enter your last name.',
					hasError: false,
					errorMessage: 'Please use only letters, apostrophes and hyphens, and limit your entry to between 2 and 50 characters.',
					value: '',
					type: 'text',
					isErrorOnBD: false,
					errorMessageFromBD: '',
				},
			],
		},
		8: {
			header: '',
			disabled: '',
			options: [
				{
					key: this.uniqueId(),
					caption: 'Email Address',
					isEmpty: false,
					emptyErrorMessage: 'Please enter your email.',
					hasError: false,
					errorMessage: 'Please enter your email address using the format email@example.com.',
					value: '',
					type: 'email',
					isErrorOnBD: false,
					errorMessageFromBD: '',
				},
			],
		},
		9: {
			header: 'Your email address (example@example.com) will be your Username.',
			disabled: 'disabled',
			svgURL: `${ASSETS}/assets/mark.txt#mark`,
			isStandartServerError: false,
			serverErrorMessage: '',
			isError: false,
			options: [
				{
					key: this.uniqueId(),
					caption: 'Password',
					isEmpty: false,
					emptyErrorMessage: 'Please enter a password.',
					hasError: false,
					errorMessage: 'Your password must include at least 8 characters consisting of at least 1 uppercase letter, 1 lowercase letter, and 1 number.',
					value: '',
					type: 'password',
				},
				{
					key: this.uniqueId(),
					caption: 'Re-enter Password',
					isEmpty: false,
					emptyErrorMessage: 'Please enter a password.',
					hasError: false,
					errorMessage: "Your passwords don't match.",
					value: '',
					type: 'password',
				},
			],
		},
	};

	// end setup data

	@track data = this.assets[this.currentSlide];
	confirmButtonStyle = 'confirmButton ' + this.assets[this.currentSlide].disabled;
	disabled = 'disabled';

	connectedCallback() {
		// restore data from cookies if the user accidentally closed window from slide 1 to 8

		if (this.rId !== '') {
			this.isFirstLoad = true;
			this.currentSlide = 9;
			this.confirmButtonStyle = 'confirmButton disabled';
			this.disabled = 'disabled';
			this.assets[this.currentSlide].header = this.headerSlideNine;
			this.setToggels();
			this.isShowButtonBack = false;
			this.data = { ...this.assets[this.currentSlide] };
			return;
		}
		if (document.cookie.split(';').length === 1) return;

		const saveObj = {};
		document.cookie.split(';').forEach((item) => {
			const pair = item.split('=');
			saveObj[pair[0].trim()] = pair[1];
		});

		if (saveObj.isRefinance === '' && saveObj.currentSlide === '') return;

		for (let i = 0; i <= 6; i++) {
			let tempIndex = this.getIndexByValue(i, saveObj[i]);
			if (tempIndex != null) {
				const copy = { ...this.assets[i] };
				copy.disabled = '';
				copy.options[tempIndex].selected = true;
				this.assets[i] = copy;
			}
		}

		const nameValues = saveObj[7].split('&');
		this.assets['7'].options[0].value = nameValues[0];
		this.assets['7'].options[1].value = nameValues[1];

		this.assets['8'].options[0].value = saveObj[8];

		this.assets.currentSlide = +saveObj.currentSlide;
		this.currentSlide = +saveObj.currentSlide;
		this.isRefinance = saveObj.isRefinance === 'true';

		this.confirmButtonStyle = 'confirmButton ';
		this.disabled = this.assets[this.currentSlide].disabled;

		this.setToggels();
		this.isShowButtonBack = this.currentSlide === 0 || this.currentSlide === 9 ? false : true;
		this.isFirstLoad = true;
		this.data = { ...this.assets[this.currentSlide] };
	}

	renderedCallback() {
		// update circle bar
		if (this.isFirstLoad) {
			this.isFirstLoad = false;
			this.template.querySelector('c-circle-bar').changeSlide(this.currentSlide);
		}
	}

	// change slide
	change = (e) => {
		e.target.blur();
		if (e.currentTarget.dataset.disabled === 'disabled') return;
		if (this.checkError(e)) return;
		if (this.currentSlide === 8 && e.currentTarget.dataset.change === 'increase') {
			APEX_CHECK_EMAIL({ wrapper: { email: this.assets[this.assets.currentSlide].options[0].value } })
				.then((result) => {
					if (result) {
						this.onSubmit();
					} else {
						this.assets[this.assets.currentSlide].options[0].isErrorOnBD = true;
						this.assets[this.assets.currentSlide].options[0].errorMessageFromBD = 'This email address is already in use';
						this.data = { ...this.assets[this.currentSlide] };
					}
				})
				.catch((err) => {
					this.assets[this.assets.currentSlide].options[0].isErrorOnBD = true;
					this.assets[this.assets.currentSlide].options[0].errorMessageFromBD = err.body.message;
					this.data = { ...this.assets[this.currentSlide] };
				});

			return;
		}
		if (this.currentSlide === 9 && e.currentTarget.dataset.change === 'increase') {
			this.setPassword();
			return;
		}

		if (this.currentSlide === 2 && this.isRefinance && e.currentTarget.dataset.change === 'increase') {			
			this.currentSlide = 5;
		} else if (this.currentSlide === 5 && this.isRefinance && e.currentTarget.dataset.change === 'decrease') {
			this.currentSlide = 2;
		} else {
			this.currentSlide += e.currentTarget.dataset.change === 'increase' ? 1 : -1;
		}
		this.assets.currentSlide = this.currentSlide;
		this.template.querySelector('c-circle-bar').changeSlide(this.currentSlide);
		this.setToggels();
		this.confirmButtonStyle = 'confirmButton ' + this.assets[this.currentSlide].disabled;
		this.disabled = this.assets[this.currentSlide].disabled;
		this.data = { ...this.assets[this.currentSlide] };
		this.setSavePoint();
	};

	uniqueId() {
		return '_' + Math.random().toString(36).substr(2, 10);
	}

	// get user choice
	selectOption(e) {
		this.assets[this.currentSlide].options.map((item, index) => {
			if (index === +e.detail) {
				item.selected = true;
				return item;
			} else {
				item.selected = false;
				return item;
			}
		});

		if (this.currentSlide === 0) {
			if (+e.detail === 1) {
				this.isRefinance = false;				
			} else {
				this.isRefinance = true;				
			}
		}

		this.confirmButtonStyle = 'confirmButton ';
		this.assets[this.currentSlide].disabled = '';
		this.disabled = '';

		this.data = { ...this.assets[this.currentSlide] };
		this.setSavePoint();
	}

	// get user data from the input fields
	typeOnField(e) {
		const index = +e.detail.index;
		let value = e.detail.value;

		this.assets[this.currentSlide].options[index].value = value;
		this.assets[this.currentSlide].options[index].hasError = false;
		this.assets[this.currentSlide].options[index].isEmpty = false;

		this.setSavePoint();

		this.data = { ...this.assets[this.currentSlide] };
	}

	// Parse data from custom label
	parseStr(str) {
		const result = str.split(';');
		if (result.length < 2) result.push('');
		if (result.length > 2) result[1] = [result[1], result[2]].join(';');
		return { one: result[0], two: result[1] };
	}

	// Check input fields on errors
	checkError(e) {
		if (this.currentSlide >= 7 && e.currentTarget.dataset.change === 'increase') {
			let isEmpty = false;
			let hasError = false;
			this.assets[this.currentSlide].options.forEach((option, index) => {
				if (option.value.trim() === '') {
					isEmpty = true;
					this.assets[this.currentSlide].options[index].isEmpty = true;
				}

				if (this.currentSlide === 7) {
					if ((!checkFields.test(option.value.trim()) || option.value.trim().length < 2) && !isEmpty) {
						this.assets[this.currentSlide].options[index].hasError = true;
						hasError = true;
					} else {
						this.assets[this.currentSlide].options[index].value = option.value.trim();
					}
				} else if (this.currentSlide === 8) {
					if (!checkEmail.test(option.value.trim()) && !isEmpty) {
						this.assets[this.currentSlide].options[index].hasError = true;
						hasError = true;
					} else {
						this.assets[this.currentSlide].options[index].value = option.value.toLowerCase().trim();
					}
				} else if (this.currentSlide === 9) {
					let value = option.value;
					if (index === 0) {
						if ((!checkUpper.test(value) || !checkLower.test(value) || !checkNumber.test(value) || !(value.length > 7)) && !isEmpty) {
							this.assets[this.currentSlide].options[index].hasError = true;
							hasError = true;
						}
					} else {
						if (this.assets[this.currentSlide].options[0].value !== this.assets[this.currentSlide].options[1].value && !isEmpty) {
							this.assets[this.currentSlide].options[index].hasError = true;
							hasError = true;
						}
					}
				}
			});
			if (isEmpty || hasError) {
				this.data = { ...this.assets[this.currentSlide] };
				return true;
			}
		}
		return false;
	}

	// Checkbox logic on agreements slide
	agreeTerms(e) {
		const className = e.detail ? '' : 'disabled';
		this.confirmButtonStyle = 'confirmButton ' + className;
		this.assets[this.currentSlide].disabled = className;
		this.disabled = className;

		this.data = { ...this.assets[this.currentSlide] };
	}

	async onSubmit() {
		const newCustomerWrapper = {};
		newCustomerWrapper.typeOfLoan = this.getValue(this.assets[0]);
		newCustomerWrapper.homeDescription = this.getValue(this.assets[1]);
		newCustomerWrapper.propertyUse = this.getValue(this.assets[2]);
		newCustomerWrapper.purchaseTimeframe = this.isRefinance ? '' : this.getValue(this.assets[3]);
		newCustomerWrapper.firstTimeBuyer = this.isRefinance ? '' : this.getValue(this.assets[4]);
		newCustomerWrapper.creditProfile = this.getValue(this.assets[5]);
		newCustomerWrapper.x2andMortgage = this.getValue(this.assets[6]);
		newCustomerWrapper.firstName = this.assets[7].options[0].value;
		newCustomerWrapper.lastName = this.assets[7].options[1].value;
		newCustomerWrapper.email = this.assets[8].options[0].value;

		this.isShowSpinner = true;

		APEX_SUBMIT({ wrapper: newCustomerWrapper })
			.then(() => {
				this.isShowSpinner = false;

				if (document.cookie.split(';').length !== 1) {
					const saveObj = {};
					document.cookie.split(';').forEach((item) => {
						const pair = item.split('=');
						saveObj[pair[0].trim()] = pair[1];
					});
					for (let key in saveObj) {
						document.cookie = key + '=; path=/Ready2Fund/s';
					}
				}
				this.dispatchEvent(new CustomEvent('showverify'));
			})
			.catch((err) => {
				console.log(err);
			});
	}

	async setPassword() {
		this.isShowSpinner = true;
		this.assets[this.currentSlide].options.isStandartServerError = false;
		this.data = { ...this.assets[this.currentSlide] };
		APEX_SET_PASSWORD({
			wrapper: { recordId: this.rId, password: this.assets[this.currentSlide].options[0].value, partnerAccountId: this.paId },
		})
			.then(() => {
				this.isShowSpinner = false;
				const url = window.location.href;
				const index = url.indexOf('/s');
				window.location.assign(url.slice(0, index) + '/s/login?verify=true');
			})
			.catch((err) => {
				this.assets[this.currentSlide].isStandartServerError = true;
				this.assets[this.currentSlide].serverErrorMessage = err.body.message;
				this.isShowSpinner = false;
				this.data = { ...this.assets[this.currentSlide] };
			});
	}

	getValue(obj) {
		let result = '';
		obj.options.forEach((item) => {
			if (item.selected) {
				result = item.value;
			}
		});
		return result;
	}

	// save user data to cookies
	setSavePoint() {
		if (this.currentSlide === 9) return;
		document.cookie = '0=' + this.getValue(this.assets[0]) + '; path=/Ready2Fund/s';
		document.cookie = '1=' + this.getValue(this.assets[1]) + '; path=/Ready2Fund/s';
		document.cookie = '2=' + this.getValue(this.assets[2]) + '; path=/Ready2Fund/s';
		document.cookie = '3=' + ( this.isRefinance ? '' : this.getValue(this.assets[3]) ) + '; path=/Ready2Fund/s';
		document.cookie = '4=' + ( this.isRefinance ? '' : this.getValue(this.assets[4]) ) + '; path=/Ready2Fund/s';
		document.cookie = '5=' + this.getValue(this.assets[5]) + '; path=/Ready2Fund/s';
		document.cookie = '6=' + this.getValue(this.assets[6]) + '; path=/Ready2Fund/s';
		document.cookie = '7=' + this.assets[7].options[0].value + '&' + this.assets[7].options[1].value + '; path=/Ready2Fund/s';
		document.cookie = '8=' + this.assets[8].options[0].value + '; path=/Ready2Fund/s';

		document.cookie = 'isRefinance=' + this.isRefinance + '; path=/Ready2Fund/s';
		document.cookie = 'currentSlide=' + this.currentSlide + '; path=/Ready2Fund/s';
	}

	getIndexByValue(i, value) {
		let result = null;
		this.assets[i].options.forEach((item, index) => {
			if (item.value === value) {
				result = index;
			}
		});
		return result;
	}

	setToggels() {
		if (this.currentSlide === 0) {
			this.isShowButtonBack = false;
		} else {
			this.isShowButtonBack = true;
		}

		if (this.currentSlide === 3 && !this.isRefinance) {
			this.isShowSVGSlide = false;
			this.isShowOptionsSlide = true;
			this.IsShowFieldsSlide = false;
			this.IsShowAgreementSlide = false;
		} else if (this.currentSlide !== 3 && this.currentSlide < 7) {
			this.isShowSVGSlide = true;
			this.isShowOptionsSlide = false;
			this.IsShowFieldsSlide = false;
			this.IsShowAgreementSlide = false;
		} else if (this.currentSlide >= 7 && this.currentSlide < 9) {
			this.isShowSVGSlide = false;
			this.isShowOptionsSlide = false;
			this.IsShowFieldsSlide = true;
			this.IsShowAgreementSlide = false;
			this.isShowConfirmButton = true;
		} else if (this.currentSlide === 9) {
			this.isShowSVGSlide = false;
			this.isShowOptionsSlide = false;
			this.IsShowFieldsSlide = false;
			this.IsShowAgreementSlide = true;
			this.isShowConfirmButton = false;
		}
	}
}