import { LightningElement, api } from 'lwc';

const checkUpper = new RegExp(/[A-Z]/);
const checkLower = new RegExp(/[a-z]/);
const checkNumber = new RegExp(/[0-9]/);

export default class CommunitySiteAgreementSlide extends LightningElement {
	@api rId;
	@api assets;

	upper = 'helpCriteria';
	lower = 'helpCriteria';
	num = 'helpCriteria';
	length = 'helpCriteria';

	connectedCallback() {
		console.log('this.assets ', this.assets.header);
	}

	clickCheckbox(event) {
		this.dispatchEvent(new CustomEvent('clickcheckbox', { detail: event.currentTarget.checked }));
	}

	typeOn(e) {
		e.preventDefault();
		const value = e.currentTarget.value;
		if (+e.currentTarget.dataset.index === 0) {
			this.upper = checkUpper.test(value) ? 'helpCriteria success' : 'helpCriteria';
			this.lower = checkLower.test(value) ? 'helpCriteria success' : 'helpCriteria';
			this.num = checkNumber.test(value) ? 'helpCriteria success' : 'helpCriteria';
			this.length = value.length > 7 ? 'helpCriteria success' : 'helpCriteria';
		}
		this.dispatchEvent(
			new CustomEvent('type', {
				detail: {
					index: e.currentTarget.dataset.index,
					value: value,
				},
			}),
		);
	}
}