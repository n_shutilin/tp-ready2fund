import { LightningElement, track } from 'lwc';
import SAVE_GOVENMENT_QUESTION from '@salesforce/apex/CompleteApplication.saveGovernmentQuestion';
import SAVE_ALIMONY_PAYMENT from '@salesforce/apex/CompleteApplication.saveAlimonyPayment';

export default class CommunityApplicationGovernmentQuestions extends LightningElement {

    isOutstandingJudgmentsLabel = 'Are there any outstanding judgments against you from a debt collection lawsuit?';
    isOutstandingJudgments = '';
    isDeclaredBankruptcyLabel = 'Have you declared bankruptcy in the past seven years?';
    isDeclaredBankruptcy = '';
    isInvolvedInLawsuitLabel = 'Are you involved in a lawsuit that could result in you owing money?';
    isInvolvedInLawsuit = '';
    isDelinquentDebtLabel = 'Are you currently delinquent or in default on a Federal debt?';
    isDelinquentDebt = '';
    isBorrowingMoneyLabel = 'Are you borrowing money to buy or refinance a home, or getting money you haven\'t already told us about from another party?';
    isBorrowingMoney = '';
    isGuarantorDebtLabel = 'Are you a cosigner or guarantor on any debt or loan (not including your current mortgage, if you have one)?';
    isGuarantorDebt = '';
    isUSCitizenLabel = 'Are you a U.S. citizen?';
    isUSCitizen = '';
    isAlimonyChildSupportLabel = 'Are you legally required to pay alimony, child suppor or separate maintenance?';
    isAlimonyChildSupport = '';
    isPrimaryResidenceLabel = 'Will you home you`re refinancing be your primary residence?';
    isPrimaryResidence = '';
    isMortgageDifferentPropertyLabel = 'Will you get a mortgage on a different property on or before the closing date of the loan you`re applying for now?';
    isMortgageDifferentProperty = '';
    isOpenNewLineLabel = 'Will you open any new lines of credit on or before the closing date of the loan yout`re appluing for now?';
    isOpenNewLine = '';
    isOwnershipLastYearsLabel = 'Have you owned any percentage of other home in the last 3 years?';
    isOwnershipLastYears = '';
    alimonyWrapper = {};
    @track isShowOwnershipQuestion = false;

    @track alimonies = [];
    rerenderAlimonyScroll = false;

    applicationId = 'a005Y00002IMzApQAL';
    reoId = 'a0n5Y00001wInloQAC';
    borrowerId = 'a065Y00001uVFwMQAW';

    ethnicitValue = '';
    otherEthnicitValue ='';

    raceValue = '';
    otherRaceValue ='';

    genderValue = '';

    @track foreclosuresTypes = {
        foreclosure: {title: 'Foreclosure', isSelected: false},
        shortSale: {title: 'Short sale or pre-foreclosure sale', isSelected: false},
        conveyed: {title: 'Conveyed title in lieu of foreclosure', isSelected: false},
        noneApply: {title: 'None of these apply', isSelected: false},
    }

    @track ethnicities = [
        { value: 'Hispanic or Latino',  isChecked: false, isOther: false, },
        { value: 'Mexican',  isChecked: false, isOther: false, },
        { value: 'Puerto Rican',  isChecked: false, isOther: false, },
        { value: 'Cuban',  isChecked: false, isOther: false, },
        { value: 'Other',  isChecked: false, isOther: true, otherValue: '', placeholder : "For example: Colombian" },
        { value: 'Not Hispanic or Latino',  isChecked: false, isOther: false, },
    ];
    @track noneAllEthnicities = {value: 'I\'d rather not share this information',  isChecked: false,}

    @track races = [
        { value: 'American Indian or Alaska Native',  isChecked: false, isOther: true, otherValue: '', placeholder : "For example: Chickahominy" },
        { value: 'Asian',  isChecked: false, isOther: false, },
        { value: 'Asian Indian',  isChecked: false, isOther: false, },
        { value: 'Chinese',  isChecked: false, isOther: false, },
        { value: 'Filipino',  isChecked: false, isOther: false, },
        { value: 'Japanese',  isChecked: false, isOther: false, },
        { value: 'Korean',  isChecked: false, isOther: false, },
        { value: 'Vietnamese',  isChecked: false, isOther: false, },
        { value: 'Other Asian',  isChecked: false, isOther: true, otherValue: '', placeholder : "For example: Taiwanese" },
        { value: 'Black or African American',  isChecked: false, isOther: false, },        
        { value: 'Native Hawaiian or Other Pacific Islander',  isChecked: false, isOther: false, },        
        { value: 'Native Hawaiian',  isChecked: false, isOther: false, },        
        { value: 'Guamanian or Chamorro',  isChecked: false, isOther: false, },        
        { value: 'Samoan',  isChecked: false, isOther: false, },    
        { value: 'Other Pacific Islander',  isChecked: false, isOther: true, otherValue: '', placeholder : "For example: Tongan" },    
        { value: 'White',  isChecked: false, isOther: false, }, 
    ];
    @track noneAllRaces = {value: 'I\'d rather not share this information',  isChecked: false,}

    @track genders = [
        { value: 'Male',  isChecked: false, isOther: false, },
        { value: 'Female',  isChecked: false, isOther: false, },
    ];
    @track noneAllGenders = {value: 'I\'d rather not share this information',  isChecked: false,}

    renderedCallback() {
        if (this.rerenderAlimonyScroll) {
            this.rerenderAlimonyScroll = false;
            const comps = this.template.querySelectorAll('c-community-application-alimony-info');
            comps[comps.length - 1].scrollIntoView();
        }
    }

    setAnswers(event) {
        this[event.detail.name] = event.detail.value;
        this.isShowOwnershipQuestion = this.isPrimaryResidence === true ? true : false;
        if( event.detail.name === 'isAlimonyChildSupport') {
            this.alimonies = this.isAlimonyChildSupport === true ? [this.createNewAlimonyObject()] : [];
        }
    }

    setForeclosureTypes(event) {
        for (let key in this.foreclosuresTypes) {
            if (key === event.currentTarget.dataset.name) {
                this.foreclosuresTypes[key].isSelected = !this.foreclosuresTypes[key].isSelected;
            } 
        }
        console.log('this.foreclosuresTypes ', this.foreclosuresTypes);
    }

    setEthnicit(event) {        
        if (event.detail.index === undefined) {
            this.ethnicities.forEach(item => {
                item.isChecked = false;  
                if ( item.otherValue !== undefined) {
                    item.otherValue = '';
                }                  
            });
            this.ethnicitValue = 'I\'d rather not share this information';
            this.otherEthnicitValue = '';
            this.noneAllEthnicities = {...this.noneAllEthnicities, isChecked: !this.noneAllEthnicities.isChecked}; 
            //this.noneAllEthnicities.isChecked  = !this.noneAllEthnicities.isChecked;
        } else {
            this.ethnicities.forEach((item, index) => {
                if (index === +event.detail.index) {
                    if (event.detail.otherValue) {
                        item.otherValue = event.detail.otherValue;
                        this.otherEthnicitValue = event.detail.otherValue;
                    } else {
                        item.isChecked = !item.isChecked;
                        this.ethnicitValue = item.value;                                               
                    }                                       
                } else {
                    item.isChecked = false;                    
                    if ( item.otherValue !== undefined) {
                        item.otherValue = '';
                        this.otherEthnicitValue = event.detail.otherValue;
                    }
                }
            });
            this.noneAllEthnicities = {...this.noneAllEthnicities, isChecked: false};  
        }
        console.log('this.ethnicitValue ', this.ethnicitValue);        
        console.log('this.otherEthnicitValue ', this.otherEthnicitValue);        
    }

    setRace(event) {
        //console.log(JSON.parse(JSON.stringify(event.detail)));
        if (event.detail.index === undefined) {
            this.races.forEach(item => {
                item.isChecked = false;  
                if ( item.otherValue !== undefined) {
                    item.otherValue = '';
                }                  
            });
            this.raceValue = 'I\'d rather not share this information';
            this.otherRaceValue = '';
            this.noneAllRaces = {...this.noneAllRaces, isChecked: !this.noneAllRaces.isChecked}; 
        } else {
            this.races.forEach((item, index) => {
                if (index === +event.detail.index) {
                    if (event.detail.otherValue) {
                        item.otherValue = event.detail.otherValue;
                        this.otherRaceValue = event.detail.otherValue;
                    } else {
                        item.isChecked = !item.isChecked;
                        this.raceValue = item.value;                                               
                    }                                       
                } else {
                    item.isChecked = false;                    
                    if ( item.otherValue !== undefined) {
                        item.otherValue = '';
                        this.otherRaceValue = event.detail.otherValue;
                    }
                }
            });
            this.noneAllRaces = {...this.noneAllRaces, isChecked: false};  
        }
        console.log('this.raceValue ', this.raceValue);        
        console.log('this.otherRaceValue ', this.otherRaceValue);          
    }

    setGender(event) {
        //console.log(JSON.parse(JSON.stringify(event.detail)));
        if (event.detail.index === undefined) {
            this.genders.forEach(item => {
                item.isChecked = false;  
            });
            this.genderValue = 'I\'d rather not share this information';
            this.noneAllGenders = {...this.noneAllGenders, isChecked: !this.noneAllGenders.isChecked}; 
        } else {
            this.genders.forEach((item, index) => {
                if (index === +event.detail.index) {                   
                        item.isChecked = !item.isChecked;
                        this.genderValue = item.value;   
                } else {
                    item.isChecked = false; 
                }
            });
            this.noneAllGenders = {...this.noneAllGenders, isChecked: false};  
        }     
        console.log('this.genderValue ', this.genderValue);  
    }

    generateUniqueKey() {
        return '_' + Math.random().toString(36).substr(2, 10);
    }

    saveGovernmentQuestion() {
        const governmentWrapper = {};
        governmentWrapper.applicationId = this.applicationId;
        governmentWrapper.borrowerId = this.borrowerId;
        governmentWrapper.outstandingJudgments = this.isOutstandingJudgments;
        governmentWrapper.bankruptcy = this.isDeclaredBankruptcy;
        governmentWrapper.involvedInLawsuit = this.isInvolvedInLawsuit;
        governmentWrapper.currentlyDelinquent = this.isDelinquentDebt;
        governmentWrapper.borrowedFunds = this.isBorrowingMoney;
        governmentWrapper.coMakerOnNote = this.isGuarantorDebt;
        governmentWrapper.usCitizen = this.isUSCitizen;
        governmentWrapper.requiredAlimony = this.isAlimonyChildSupport;
        governmentWrapper.isPrimaryResidence = this.isPrimaryResidence;
        governmentWrapper.isMortgageDifferentProperty = this.isMortgageDifferentProperty;
        governmentWrapper.isOpenNewLine = this.isOpenNewLine;
        governmentWrapper.isOwnershipLast3Years = this.isOwnershipLastYears;
        governmentWrapper.raceValue = this.raceValue;
        governmentWrapper.ethnicitValue = this.ethnicitValue;
        governmentWrapper.genderValue = this.genderValue;

        if(this.raceValue.includes('Other')) {
            governmentWrapper.otherRaceValue = this.otherRaceValue;
        } else {
            governmentWrapper.otherRaceValue = '';
        }
        if(this.ethnicitValue.includes('Other')) {
            governmentWrapper.otherEthnicitValue = this.otherEthnicitValue;
        } else {
            governmentWrapper.otherEthnicitValue = '';
        }

        let muliresult = '';
        for (let key in this.foreclosuresTypes) {
            if (this.foreclosuresTypes[key].isSelected) {
                muliresult += `${this.foreclosuresTypes[key].title};`;
            }
        }

        governmentWrapper.last7YearsHomeIssue = muliresult;


        console.log(governmentWrapper);

        SAVE_GOVENMENT_QUESTION({ wrapper: governmentWrapper })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log('governmentWrapper');
                console.log(err);
            });

        if(this.isAlimonyChildSupport) {
            SAVE_ALIMONY_PAYMENT({ wrapper: this.alimonies, applicationId: this.applicationId })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log('alimonies')
                console.log(err);
            });
        }
    }

    addAlymony() {
        if (this.alimonies.length < 4) {
            this.alimonies.push(this.createNewAlimonyObject());
        }        
    }

    updateAlimonyObject(event) {        
        const params = event.detail;
        this.alimonies[params.index] = { ...this.alimonies[params.index], ...params.data };
        console.log('this.alimonies ', JSON.parse(JSON.stringify(this.alimonies)));
    }

    deleteAlimony(event) {
        this.alimonies.splice(+event.detail.index, 1);
        this.rerenderAlimonyScroll = true;
    }

    createNewAlimonyObject() {
        return {
            key: this.generateUniqueKey(),
            alimonyType: '',
            monthlyPay: '',
            remainingPayments: '',
            paymentReceiver: '',
        };
    }

    generateUniqueKey() {
        return '_' + Math.random().toString(36).substr(2, 10);
    }
}