import { LightningElement, wire, track } from 'lwc';
import getAccountList from '@salesforce/apex/JustForTestLghtOutClass.getAccountList';

export default class justForTestLghtCmp extends LightningElement {

    @track accounts;
    @track error;
    handleLoad() {
        getAccountList()
            .then(result => {
                console.log('~~~~~~~~~~~~'  + result);
                this.accounts = result;
            })
            .catch(error => {
                console.log('~~~~~~~~~~~~'  + error);
                this.error = error;
            });
    }

}