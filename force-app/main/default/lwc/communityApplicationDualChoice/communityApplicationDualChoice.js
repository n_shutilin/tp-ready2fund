import { LightningElement, api } from 'lwc';

export default class CommunityApplicationDualChoice extends LightningElement {

    @api label;
    @api defaultValue;
    @api actionName;
    @api variableName;

    makeChoice(event) {
        event.currentTarget.parentNode.querySelectorAll('.choice').forEach( item => {
            item.classList.remove('selected');
        });
        event.currentTarget.classList.add('selected');
        this.dispatchEvent(new CustomEvent( this.actionName , { 
            detail : { 
                name: this.variableName,
                value: event.currentTarget.dataset.name === 'true',
            }
        }));
    }

    renderedCallback() {
        this.template.querySelectorAll('.choice').forEach( item => {
            const value = this.defaultValue ? 'true' : 'false';
            if (item.dataset.name === value) {
                item.classList.add('selected');
            } else {
                item.classList.remove('selected');
            }
           /*  if (item.dataset.name == this.defaultValue) {
                item.classList.add('selected');
            } else {
                item.classList.remove('selected');
            } */
        });
    }
}