import { LightningElement, api, track } from 'lwc';

export default class CommunityApplicationChooseOption extends LightningElement {
    @api options;
    @api header;
    @api actionName;
    @api noneAll;

       

    selectItem(event) {
        if (event.currentTarget.dataset.name === 'noneAll') {
            this.noneAll = !this.noneAll;
        }
        this.dispatchEvent(new CustomEvent( this.actionName, { 
            detail : {
                index: event.currentTarget.dataset.index,
                value : event.currentTarget.dataset.name,
            }
        }));
    }

    setOther(event) {
        this.dispatchEvent(new CustomEvent( this.actionName, { 
            detail : {
                index: event.currentTarget.dataset.index,
                value : event.currentTarget.dataset.name,
                otherValue: event.currentTarget.value,
            }
        }));
    }

    toggleMenu(event) {
        event.preventDefault();
        event.currentTarget.querySelector('svg').classList.toggle('open');
        let inputCount = 0;
        this.options.forEach(item => {
            if (item.placeholder !== undefined) {
                inputCount += 55;
            }
        });
        event.currentTarget.nextSibling.style.height = event.currentTarget.nextSibling.style.height === '' 
            ? `${inputCount + 33*this.options.length}px` 
            : '';
    }

}