import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import UPLOAD_FILES from '@salesforce/apex/CompleteApplication.uploadFiles';


const MAX_FILE_SIZE = 4500000;

export default class CommunityApplicationUploadFiles extends LightningElement {

    applicationId = '';
    isShowSpinner = false;

    bankStatementsStatus = '';
    incomeVerificationStatus = '';
    employmentVerificationStatus = '';
    driverLicenseStatus = '';

    bankStatements = [];
    incomeVerification = [];
    employmentVerification = [];
    driverLicense = [];

    connectedCallback() {
        this.bankStatementsStatus = 'MISSING';
        this.incomeVerificationStatus = 'RECIEVED';
        this.employmentVerificationStatus = 'APPROVED';
        this.driverLicenseStatus = 'APPROVED';
        this.applicationId = 'a005Y00002IMzApQAL';
    }

    toggleMenu(event) {
        event.preventDefault();
        event.currentTarget.querySelector('svg').classList.toggle('open');
        event.currentTarget.parentNode.classList.toggle('open');
    }

    selectFiles(event) {
        this[event.currentTarget.dataset.name] = []; // delete previous files
        if (event.currentTarget.files.length > 4) {
            this.dispatchEvent(new ShowToastEvent(
                {
                    title: 'Error',
                    message: `The maximum number of files is 4. You selected ${event.currentTarget.files.length}`,
                    variant: 'error',
                }
            ));
            event.currentTarget.value = '';
            return;
        }

        let hasError = false;
        [...event.currentTarget.files].forEach(item => {
            if (item.size > MAX_FILE_SIZE) {
                hasError = true;
                this.dispatchEvent(new ShowToastEvent(
                    {
                        title: 'Error',
                        message: `File size cannot exceed ${MAX_FILE_SIZE} bytes.\n Selected ${item.name} file size: ${item.size}`,
                        variant: 'error',
                    }
                ));
            } else {
                this[event.currentTarget.dataset.name].push({ type: event.currentTarget.dataset.name, file: item });
            }
        });

        if (hasError) {
            event.currentTarget.value = '';
        }
    }

    readFile(obj) {
        const { type, file } = obj; 
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReader.onerror = () => reject(fileReader.error);
          fileReader.onload = () => resolve({ fileName: file.name, base64: fileReader.result.split(',')[1], contentType:  file.type, type });
          fileReader.readAsDataURL(file);
        });


    }

    async saveFiles() {
        try {
            this.isShowSpinner = true;
            const allFiles = [...this.bankStatements, ...this.incomeVerification, ...this.employmentVerification, ...this.driverLicense];
            const filesArr = await Promise.all(allFiles.map(item => this.readFile(item)));
            console.log('filesArr ', filesArr);

            await UPLOAD_FILES({
                wrapper : filesArr, 
                appliactionId : this.applicationId
            });

            this.isShowSpinner = false;
            console.log('success');

            /* UPLOAD_FILES({
                wrapper : filesArr, 
                appliactionId : this.applicationId
            })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log(err);
            }); */
        } catch(err) {
            console.log('saveFiles error ', err);
            this.isShowSpinner = false;
            this.dispatchEvent(new ShowToastEvent(
                {
                    title: 'Error',
                    message: 'Something went wrong. Try one more time later!',
                    variant: 'error',
                }
            ));
        }
    }

}