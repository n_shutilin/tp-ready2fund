import { LightningElement } from 'lwc';
import SAVE_LOAN from '@salesforce/apex/CompleteApplication.saveWhosOnTheLoan';

export default class CommunityApplicationLoanQuestion extends LightningElement {

    isMarried = false;
    isMarriedLabel = 'Are you married?';
    isSpouseOnLoan = false;
    isSpouseOnLoanLabel = 'Is your spouse going to be on the loan?';
    isMilitary = false;
    isMilitaryLabel = 'Is your spouse a military veteran, active-duty service member or surviving spouse?';
    firstName = '';
    lastName = '';
    email = '';
    borrowerId = 'a065Y00001uVK8kQAG';
    applicationId = 'a005Y00002IMzApQAL';

    setAnswers(event) {
        this[event.detail.name] = event.detail.value;
        if (this.isMarried === false) {
            this.isSpouseOnLoan = false;
            this.firstName = '';
            this.lastName = '';
            this.email = '';
            this.isMilitary = '';
        }
    }

    setDataField(event) {
        this[event.currentTarget.dataset.name] = event.currentTarget.value;
    }

    saveLoanInfo() {
        const loanWrapper = {};
        loanWrapper.applicationId = this.applicationId;
        loanWrapper.borrowerId = this.borrowerId;
        //loanWrapper.maritalStatus = this.isMarried;
        loanWrapper.firstName = this.firstName;
        loanWrapper.lastName = this.lastName;
        loanWrapper.militaryOrSurviving = this.isMilitary;

        SAVE_LOAN({ wrapper: loanWrapper })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log(err);
            });
    }
}