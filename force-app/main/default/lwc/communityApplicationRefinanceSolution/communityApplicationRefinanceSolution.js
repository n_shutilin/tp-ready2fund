import { LightningElement, track } from 'lwc';
import GET_REFINANCE_SOLUTION from '@salesforce/apex/CompleteApplication.getRefinanceSolutionInfo';

export default class CommunityApplicationRefinanceSolution extends LightningElement {
    isSpouse = false;
    isRecommended = false;
    creditScore = '';
    spouseCreditScore = '';
    recommendedPeriod = '';
    userGoal = '';
    rateTime = '';

    currentMonthlyPayment = 0;
    newMonthlyPayment = 0;
    principalAndInterest = 0;
    estimatedTaxes = 0;
    estimatedHomeownersInsurance = 0;
    mortgageInsurance = 0;

    currentMonthlyDifference = '';
    isNegativeDifference = false;
    fiveYearDifference = '';
    fifteenYearDifference = '';
    thirtyYearDifference = '';

    @track terms = [];
    selectedTerm = '';

    fixedRate = '';
    apr = '';
    estimatedClosingCosts = '';

    cashDueClosing = '';
    newLoanAmount = '';
    estimatedAmount = '';
    currentLoanAmount = '';
    loanceLLC = '';
    estimatedBeforeClosing = '';
    thirdPartyFees = '';
    discountPoints = '';
    estimatedEscrowAccount = '';
    estimatedPerDiemInterest = '';
    applicationId = 'a005Y00002IMzApQAL';

    userName = '';
    spouseName = '';

    interestRateLockPeriod = '';
    fundsNeededReserve = 0;
    loanValueRatio = 0;
    loanType = '';
    points = 0;

    combineClassName = 'bigHeader dropDownContainer';


    connectedCallback() {
        this.isSpouse = true;
        this.isRecommended = true;

        this.creditScore = 790;
        this.spouseCreditScore = 758;
        this.recommendedPeriod = '30-Year Fixed';
        this.userGoal = 'Lower My Monthly Payment';
        this.rateTime = '12:04PM (EDT) on 05/14/2021';

        this.currentMonthlyPayment = 2683;
        this.newMonthlyPayment = 2177.8;
        this.principalAndInterest = 2094.88;
        this.estimatedTaxes = 0;
        this.estimatedHomeownersInsurance = 0;
        this.mortgageInsurance = 82.92;


        this.currentMonthlyDifference = -505.20;
        this.isNegativeDifference = this.currentMonthlyDifference < 0;  

        this.fiveYearDifference = 0;
        this.fifteenYearDifference = 90936;
        this.thirtyYearDifference = 181827;

        this.terms.push({ name: '30 Years', value: '30 Years', isSelected: false });
        this.terms.push({ name: '20 Years', value: '20 Years', isSelected: false });
        this.terms.push({ name: '15 Years', value: '15 Years', isSelected: false });

        this.fixedRate = '3.14';
        this.apr = '3.312';
        this.estimatedClosingCosts = '10875.71';

        this.cashDueClosing = 0;
        this.newLoanAmount = 486167;
        this.estimatedAmount = 470800;
        this.currentLoanAmount = 468117;
        this.loanceLLC = 468117;
        this.estimatedBeforeClosing = 2683;
        this.thirdPartyFees = 2975.5;
        this.discountPoints = 0;
        this.estimatedEscrowAccount = 0;
        this.estimatedPerDiemInterest = 666.35;

        this.userName = 'Brett' + '\'s';
        this.spouseName = 'Emily' + '\'s';

        this.interestRateLockPeriod = '45 days';
        this.fundsNeededReserve = 0;
        this.loanValueRatio = 94;
        this.loanType = '30-Year Fixed';
        this.points = 0;

        this.combineClassName = this.isSpouse ? 'bigHeader includeSpouse dropDownContainer' : 'bigHeader dropDownContainer';

        GET_REFINANCE_SOLUTION({
            wrapper : {
                applicationId : this.applicationId
            }})
        .then((result) => {
            console.log('success', result);

            this.creditScore = result.get('creditScore');
            this.spouseCreditScore = result.get('spouseCreditScore');
            this.recommendedPeriod = result.get('term') + ' Fixed';

            this.userName = result.get('userName') + '\'s';
            this.spouseName = result.get('spouseName') + '\'s';
            this.isSpouse = (result.get('isSpouse') === 'true');

            this.currentMonthlyPayment = result.get('currentMonthlyPayment');
            this.newMonthlyPayment = result.get('newMonthlyPayment');
            
            this.principalAndInterest = result.get('newMonthlyPayment');
            this.estimatedTaxes = result.get('estimatedTaxes');
            this.estimatedHomeownersInsurance = result.get('estimatedHomeownersInsurance');
            this.mortgageInsurance = result.get('mortgageInsurance');


            this.currentMonthlyDifference = result.get('currentMonthlyDifference');
            this.isNegativeDifference = this.currentMonthlyDifference < 0;  

            /*this.fiveYearDifference = 0;
            this.fifteenYearDifference = 90936;
            this.thirtyYearDifference = 181827;*/

            this.fixedRate = result.get('fixedRateAPR');
            this.apr = result.get('fixedRateAPR');
            this.estimatedClosingCosts = result.get('estimatedClosingCosts');

            this.cashDueClosing = result.get('cashDueatClosing');
            this.newLoanAmount = result.get('newLoanAmount');

            this.estimatedAmount = result.get('estimatedAmount');
            this.currentLoanAmount = result.get('currentLoanAmount');
            /*this.loanceLLC = 468117;
            this.estimatedBeforeClosing = 2683;
            this.thirdPartyFees = 2975.5;
            this.discountPoints = 0;
            this.estimatedEscrowAccount = 0;
            this.estimatedPerDiemInterest = 666.35;*/

            this.points = result.get('points');
            this.interestRateLockPeriod = result.get('interestRateLockPeriod');
            this.fundsNeededReserve = result.get('fundsNeededinReserve');
            this.loanValueRatio = result.get('loantoValueRatio');
            this.loanType = result.get('term') + ' Fixed';

        })
        .catch((err) => {
            console.log(err);
        });
    }

    toggleMenu(event) {
        event.preventDefault();
        event.currentTarget.parentNode.classList.toggle('open');
        event.currentTarget.querySelector('svg').classList.toggle('open');
    }


    toggleInnerMenu(event) {
        event.preventDefault();
        event.currentTarget.parentNode.parentNode.parentNode.parentNode.classList.toggle('innerOpen');
    }

    selectTerm(event) {
        this.terms.forEach(item => {
            if (item.value === event.currentTarget.value) {
                item.isSelected = true;
                this.selectedTerm = item.value;
            } else {
                item.isSelected = false;
            }
        });
        console.log(this.terms);
    }

}