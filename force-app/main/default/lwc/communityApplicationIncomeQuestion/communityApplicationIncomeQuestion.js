import { LightningElement, track } from 'lwc';
import SAVE_INCOME from '@salesforce/apex/CompleteApplication.saveIncome';
import SAVE_ADDITIONAL_INCOME from '@salesforce/apex/CompleteApplication.saveAdditionalIncome';

export default class CommunityApplicationIncomeQuestion extends LightningElement {
    @track currentJobs = [];
    @track previousJobs = [];
    @track socialIncomes = [];
    rerenderCurrentJobScroll = false;
    rerenderPreviousJobScroll = false;
    rerenderSocialIncomeScroll = false;
    applicationId = 'a005Y00002IMzApQAL';
    borrowerId = 'a065Y00001uVFwMQAW';
    spouseId = 'a065Y00001uVK8kQAG';

    connectedCallback() {
        this.currentJobs = [
            {
                isEditMode: false,
                isNew: false,
                key: this.generateUniqueKey(),
                employerName: 'Touchpoint',
                employerPhoneOne: '333',
                employerPhoneTwo: '222',
                employerPhoneThree: '1111',
                jobTitle: 'Driver',
                jobType: 'Full-time',
                startMonth: '2',
                startYear: '2020',
                baseIncome: '20000',
                bonus: {
                    isSelected: true,
                    primaryYear: '',
                    primaryAmount: 0,
                    secondaryYear: '2020',
                    secondaryAmount: 50000,
                },
                commission: {
                    isSelected: false,
                    primaryYear: '',
                    primaryAmount: 0,
                    secondaryYear: '',
                    secondaryAmount: 0,
                },
                overtime: {
                    isSelected: true,
                    primaryYear: '2021',
                    primaryAmount: 1000,
                    secondaryYear: '',
                    secondaryAmount: 0,
                },
                isEmployerFamilyMember: true,
            },
        ];
        this.previousJobs = [{
            isEditMode: false,
            isNew: false,
            key: this.generateUniqueKey(),
            employerName: 'Touchpoint',
            startMonth: '02',
            startYear: '2020',
            endMonth: '03',
            endYear: '2021',
        }];
        this.socialIncomes = [{
            isEditMode: false,
            isNew: false,
            key: this.generateUniqueKey(),
            yearIncome: 20000,
            socialIncomeType: 'I\'m Self-Employed',
        }];
    }

    renderedCallback() {
        if (this.rerenderCurrentJobScroll) {
            this.rerenderCurrentJobScroll = false;
            if (this.currentJobs.length === 0) {
                this.template.querySelector('.currentJobAnchor').scrollIntoView();
            } else {
                const comps = this.template.querySelectorAll('c-community-application-current-job');
                comps[comps.length - 1].scrollIntoView();
            }
        } else if (this.rerenderPreviousJobScroll) {
            this.rerenderPreviousJobScroll = false;
            if (this.previousJobs.length === 0) {
                this.template.querySelector('.previousJobAnchor').scrollIntoView();
            } else {
                const comps = this.template.querySelectorAll('c-community-application-previous-job');
                comps[comps.length - 1].scrollIntoView();
            }
        } else if (this.rerenderSocialIncomeScroll) {
            this.rerenderSocialIncomeScroll = false;
            if (this.socialIncomes.length === 0) {
                this.template.querySelector('.socialIncomeAnchor').scrollIntoView();
            } else {
                const comps = this.template.querySelectorAll('c-community-application-social-income');
                comps[comps.length - 1].scrollIntoView();
            }
        }
    }

    addItem(event) {
        if (event.currentTarget.dataset.name === 'currentJob') {
            this.currentJobs.push(this.createNewCurrentJobObject());
        } else if (event.currentTarget.dataset.name === 'previousJob') {
            this.previousJobs.push(this.createNewPreviousJobObject());
        } else if (event.currentTarget.dataset.name === 'moreIncome') {
            this.socialIncomes.push(this.createNewSocialIncomeObject());
        }
    }

    createNewCurrentJobObject() {
        /* return {
            isEditMode: true,
            isNew: true,
            key: this.generateUniqueKey(),
            employerName: '',
            employerPhoneOne: '',
            employerPhoneTwo: '',
            employerPhoneThree: '',
            jobTitle: '',
            jobType: '',
            startMonth: '',
            startYear: '',
            baseIncome: '',
            bonus: '',
            commission: '',
            overtime: '',
            isEmployerFamilyMember: false,
        }; */
        return {
            isEditMode: true,
            isNew: true,
            key: this.generateUniqueKey(),
            employerName: '',
            employerPhoneOne: '',
            employerPhoneTwo: '',
            employerPhoneThree: '',
            jobTitle: '',
            jobType: '',
            startMonth: '',
            startYear: '',
            baseIncome: '',
            bonus: {
                isSelected: false,
                primaryYear: '',
                primaryAmount: 0,
                secondaryYear: '',
                secondaryAmount: 0,
            },
            commission: {
                isSelected: false,
                primaryYear: '',
                primaryAmount: 0,
                secondaryYear: '',
                secondaryAmount: 0,
            },
            overtime: {
                isSelected: false,
                primaryYear: '',
                primaryAmount: 0,
                secondaryYear: '',
                secondaryAmount: 0,
            },
            isEmployerFamilyMember: false,
        };
    }

    createNewPreviousJobObject() {
        return {
            isEditMode: true,
            isNew: true,
            key: this.generateUniqueKey(),
            employerName: '',
            startMonth: '',
            startYear: '',
            endMonth: '',
            endYear: '',
        };
    }

    createNewSocialIncomeObject() {
        return {
            isEditMode: true,
            isNew: true,
            key: this.generateUniqueKey(),
            yearIncome: '',
            socialIncomeType: '',
        };
    }

    updateJobObject(event) {
        const params = event.detail;
        console.log('params ', params);
        this.currentJobs[params.index] = { ...this.currentJobs[params.index], ...params.data };
        console.log('this.currentJobs ', JSON.parse(JSON.stringify(this.currentJobs)));
    }

    cancelCurrentJob(event) {
        if (this.currentJobs[event.detail.index].isNew) {
            this.currentJobs.splice(event.detail.index, 1);
        } else {
            this.currentJobs[event.detail.index] = { ...this.currentJobs[event.detail.index], isEditMode: false };
        }
        this.rerenderCurrentJobScroll = true;
    }

    updatePreviousJobObject(event) {
        const params = event.detail;
        this.previousJobs[params.index] = { ...this.previousJobs[params.index], ...params.data };
        console.log('this.previousJobs ', JSON.parse(JSON.stringify(this.previousJobs)));
    }

    cancelPreviousJob(event) {
        if (this.previousJobs[event.detail.index].isNew) {
            this.previousJobs.splice(event.detail.index, 1);
        } else {
            this.previousJobs[event.detail.index] = { ...this.previousJobs[event.detail.index], isEditMode: false };
        }
        this.rerenderPreviousJobScroll = true;
    }

    updateSocialIncomeObject(event) {
        const params = event.detail;
        this.socialIncomes[params.index] = { ...this.socialIncomes[params.index], ...params.data };
        console.log('this.socialIncomes ', JSON.parse(JSON.stringify(this.socialIncomes)));
    }

    cancelSocialIncomeObject(event) {
        if (this.socialIncomes[event.detail.index].isNew) {
            this.socialIncomes.splice(event.detail.index, 1);
        } else {
            this.socialIncomes[event.detail.index] = { ...this.socialIncomes[event.detail.index], isEditMode: false };
        }
        this.rerenderSocialIncomeScroll = true;
    }

    generateUniqueKey() {
        return '_' + Math.random().toString(36).substr(2, 10);
    }

    saveIncomeInfo() {

        if (this.currentJobs) {

            console.log(this.currentJobs);

            SAVE_INCOME({ wrapper: this.currentJobs, applicationId: this.applicationId, borrowerId: this.borrowerId, isCurrent: true })
                .then(() => {
                    console.log('success');
                })
                .catch((err) => {
                    console.log(err);
                });
        }


        if (this.previousJobs) {

            console.log(this.previousJobs);

            SAVE_INCOME({ wrapper: this.previousJobs, applicationId: this.applicationId, borrowerId: this.borrowerId, isCurrent: false })
                .then(() => {
                    console.log('success');
                })
                .catch((err) => {
                    console.log(err);
                });
        }

        if (this.socialIncomes) {

            console.log(this.socialIncomes);

            SAVE_ADDITIONAL_INCOME({ wrapper: this.socialIncomes, borrowerId: this.borrowerId })
                .then(() => {
                    console.log('success');
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }

    switchToEdit(event) {
        //this.currentJobs[event.detail.index] = { ...this.currentJobs[event.detail.index], isEditMode: true };
        this[event.detail.target][event.detail.index] = { ...this[event.detail.target][event.detail.index], isEditMode: true };
    }
}