import { LightningElement, api } from 'lwc';

export default class CommunitySiteSvgSlide extends LightningElement {
	@api assets;

	renderedCallback() {
		this.template.querySelectorAll('.svgContainer').forEach((elem) => {
			// delete old listener
			elem.removeEventListener('mouseover', this.clearFocus);
			elem.addEventListener('mouseover', this.clearFocus);
		});
	}

	selectOption(e) {
		e.preventDefault();
		this.sendEvent(e);
	}

	pressKey(e) {
		if (e.keyCode === 13 || e.keyCode === 32) {
			this.sendEvent(e);
		}
	}

	sendEvent(e) {
		this.dispatchEvent(new CustomEvent('selected', { detail: e.currentTarget.dataset.index }));
	}

	clearFocus = () => {
		this.template.querySelectorAll('.svgContainer').forEach((elem) => elem.blur());
	};
}