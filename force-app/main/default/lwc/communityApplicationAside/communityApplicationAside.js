import { LightningElement, api, track } from 'lwc';

export default class CommunityApplicationAside extends LightningElement {

    @api currentIndex;
    @api maxIndexPage;
    @api steps;
    //currentPage = this.currentIndex + 1;

    connectedCallback() {
        this.currentPage = this.currentIndex + 1;
    }

    renderedCallback() {
        this.currentPage = this.currentIndex + 1;
    }

    showProgressBar(event) {
        event.preventDefault();
        event.currentTarget.classList.toggle('open');
        this.template.querySelector('.stepsContainer').classList.toggle('open');
    }

    selectStep(event) {
        if (event.currentTarget.className === 'future') return;
        this.dispatchEvent(new CustomEvent('selectstep', { detail: { index: event.currentTarget.dataset.index }}));
    }    
}