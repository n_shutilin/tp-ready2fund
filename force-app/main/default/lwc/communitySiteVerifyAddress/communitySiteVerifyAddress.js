import { LightningElement } from 'lwc';
import EMAIL_PICTURES from '@salesforce/resourceUrl/emailPictures';
import APEX_RESENT_EMAIL from '@salesforce/apex/SubmitApplication.resendEmail';

export default class CommunitySiteVerifyAddress extends LightningElement {
	imgURL = `${EMAIL_PICTURES}/emailPictures/sendEmail.jpg`;

	resendEmail(e) {
		e.preventDefault();

		if (document.cookie.split(';').length === 1) return;

		const saveObj = {};
		document.cookie.split(';').forEach((item) => {
			const pair = item.split('=');
			saveObj[pair[0].trim()] = pair[1];
		});

		if (saveObj.isRefinance === '' && saveObj.currentSlide === '') return;

		const newCustomerWrapper = {};
		newCustomerWrapper.typeOfLoan = saveObj[0];
		newCustomerWrapper.homeDescription = saveObj[1];
		newCustomerWrapper.propertyUse = saveObj[2];

		if (saveObj.isRefinance === 'true') {
			newCustomerWrapper.purchaseTimeframe = saveObj[3];
			newCustomerWrapper.firstTimeBuyer = saveObj[4];
		} else {
			newCustomerWrapper.creditProfile = saveObj[3];
			newCustomerWrapper.x2andMortgage = saveObj[4];
		}

		const nameValues = saveObj[5].split('&');
		newCustomerWrapper.firstName = nameValues[0];
		newCustomerWrapper.lastName = nameValues[1];
		newCustomerWrapper.email = saveObj[6];
		newCustomerWrapper.password = saveObj[7];

		APEX_RESENT_EMAIL({ wrapper: newCustomerWrapper })
			.then(() => {
				alert("We've sent you an email");
			})
			.catch(() => {
				alert('An error occurred while submitting your request, please contact us or try again.');
			});
	}
}