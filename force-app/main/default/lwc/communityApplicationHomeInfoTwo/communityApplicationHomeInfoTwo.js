import { LightningElement, track, wire } from 'lwc';
import { getPicklistValues, getRecord, getFieldValue } from 'lightning/uiObjectInfoApi';
import PROPERTY_FIELD from '@salesforce/schema/REO__c.Property_Type__c';
import OCCUPANCY_FIELD from '@salesforce/schema/REO__c.Occupancy__c';
import SAVE_HOME_INFO_SECOND_PART from '@salesforce/apex/CompleteApplication.saveHomeInfoSecondPart';
import GET_HOME_INFO from '@salesforce/apex/CompleteApplication.getHomeInfo';

const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationHomeInfoTwo extends LightningElement {
    currentPropertyAddress = '';
    //currentPropertyAddress = '9092 Madeline Dr Huntington Beach, CA 92646-3418';
    @track properties = [];
    @track usageTypes = [];
    operateBusinessOnProperty = false;
    operateBusinessOnPropertyLabel = 'Do you plan on operating a business in this home or on the property?';
    oropertyType = '';
    occupancy = '';
    taxExpense = '';
    insuranceProvider = '';
    insuranceExpense = '';
    originalPurchasePrice = '';
    yearBuilt = '';
    startLivingMonth = '';
    startLivingYear = '';
    applicationId = 'a005Y00002IMzApQAL';
    reoId = 'a0n5Y00001wJnaPQAS';
    reoRecord;
    reoInfo = {};

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: PROPERTY_FIELD,
    }) propertiyField({ error, data }) {
        if (data) {
            for (var i = 0; i < data.values.length; i++) {
                this.properties.push({name: data.values[i].label, value: data.values[i].value, isSelected: false });
            }
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.properties = undefined;
        }
    }

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: OCCUPANCY_FIELD,
    }) usageTypeField({ error, data }) {
        if (data) {
            for (var i = 0; i < data.values.length; i++) {
                this.usageTypes.push({name: data.values[i].label, value: data.values[i].value, isSelected: false });
            }
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.usageTypes = undefined;
        }
    }

    connectedCallback() {        
        GET_HOME_INFO({ wrapper: { reoId : this.reoId} })
            .then((result) => {
                console.log('success', result);
                this.currentPropertyAddress = result.address;
            })
            .catch((err) => {
                console.log(err);
            });
    }

    selectPropertyType(event) {
        this.oropertyType = event.currentTarget.value;
        console.log(this.oropertyType);
    }

    selectUsageType(event) {
        this.occupancy = event.currentTarget.value;
        console.log(this.occupancy);
    }

    changeOperateBusinessOnProperty(event) {
        this[event.detail.name] = event.detail.value;
    }

    setCurrencyField(event) {            
        let val = event.currentTarget.value.split('.').join('');        
        if (checkNumbers.test(val) || val === '') {
            this[event.currentTarget.dataset.name] = val;
            event.currentTarget.value = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    }

    setDateValue(event) {            
        let val = event.currentTarget.value;        
        if (checkNumbers.test(val) || val === '') {
            if (val.length <= 4 && event.currentTarget.dataset.name !== 'startLivingMonth') {
                this[event.currentTarget.dataset.name] = val;
            } else if ( (val.length <= 2 && event.currentTarget.dataset.name == 'startLivingMonth' && +val >0 && +val < 13) || val === '') {
                this[event.currentTarget.dataset.name] = val;
            } else {
                event.currentTarget.value = this[event.currentTarget.dataset.name];
            }
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name];
        }
    }

    setInsuranceProvider(event) {
        this.insuranceProvider = event.currentTarget.value;
    }

    saveHomeInfo() {
        const secondHomeInfo = {};
        secondHomeInfo.applicationId = this.applicationId;
        secondHomeInfo.reoId = this.reoId;
        secondHomeInfo.oropertyType = this.oropertyType;
        secondHomeInfo.occupancy = this.occupancy;
        secondHomeInfo.operateBusinessOnProperty = this.operateBusinessOnProperty;
        secondHomeInfo.taxExpense = this.taxExpense;
        secondHomeInfo.insuranceProvider = this.insuranceProvider;
        secondHomeInfo.insuranceExpense = this.insuranceExpense;
        secondHomeInfo.originalPurchasePrices = this.originalPurchasePrice;
        secondHomeInfo.yearBuilt = this.yearBuilt;
        secondHomeInfo.startedLivingHereMonth = this.startLivingMonth;
        secondHomeInfo.startedLivingHereYear = this.startLivingYear;

        console.log(secondHomeInfo);

        SAVE_HOME_INFO_SECOND_PART({ wrapper: secondHomeInfo })
            .then(() => {
                console.log('success');
            })
            .catch((err) => {
                console.log(err);
            });
    }
    
}