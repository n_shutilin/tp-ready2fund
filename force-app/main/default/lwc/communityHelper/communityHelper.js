import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export function showSpinner(context) {
    context.dispatchEvent(new CustomEvent('showspinner', { bubbles: true, composed: true }));
}

export function hideSpinner(context) {
    context.dispatchEvent(new CustomEvent('hidespinner', { bubbles: true, composed: true }));
}

export function showToast(context, title, message, variant) {
    context.dispatchEvent(new ShowToastEvent( { title, message, variant, } ));
}

export function showNextSlide(context) {
    context.dispatchEvent(new CustomEvent('nextslide', { bubbles: true, composed: true}));
}

export function uniqueId () {
    return '_' + Math.random().toString(36).substr(2, 10);
}