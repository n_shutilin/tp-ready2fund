import { LightningElement } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import SITE_IMGS from '@salesforce/resourceUrl/communitySiteIMG';
import APEX_LOGIN from '@salesforce/apex/SubmitApplication.login';
import ASSETS from '@salesforce/resourceUrl/communitySignInCss';

export default class CommunitySignInPage extends LightningElement {
	logoURL = SITE_IMGS + '/logo.png';
	login = '';
	password = '';
	passFieldType = 'password';
	isShowVerifyMessage = false;
	isStandardError = false;
	errorStandartMessage = '';
	isShowSpinner = true;

	connectedCallback() {
		loadStyle(this, `${ASSETS}/signin_forgot_pass/typekit.css`);
		loadStyle(this, `${ASSETS}/signin_forgot_pass/signin_forgot.css`);
		const url = new URL(window.location.href);
		if (url.searchParams.get('verify') === 'true') {
			this.isShowVerifyMessage = true;
		}
		setTimeout(() => (this.isShowSpinner = false), 500);
	}

	typeOn(e) {
		if (e.currentTarget.dataset.name === 'username') {
			this.login = e.currentTarget.value;
		} else {
			this.password = e.currentTarget.value;
		}
	}

	showPass(e) {
		this.passFieldType = e.currentTarget.checked ? 'text' : 'password';
	}

	submit() {
		this.isStandardError = false;
		const url = window.location.href;
		const index = url.indexOf('login');
		const userData = {
			username: this.login.toLocaleLowerCase().trim(),
			password: this.password,
			startUrl: 'https://ready2found-developer-edition.na156.force.com/Ready2Fund/s/',
		};
		APEX_LOGIN({ wrapper: userData })
			.then((result) => {
				location.href = result;
			})
			.catch((err) => {
				console.log(err);
				this.errorStandartMessage = err.body.message;
				this.isStandardError = true;
			});
	}

	closeVerifyMessage() {
		this.isShowVerifyMessage = false;
	}

	forgotPassword(e) {
		e.preventDefault();
		const url = window.location.href;
		const index = url.indexOf('/login');
		window.location.assign(url.slice(0, index) + '/login/ForgotPassword');
	}
}