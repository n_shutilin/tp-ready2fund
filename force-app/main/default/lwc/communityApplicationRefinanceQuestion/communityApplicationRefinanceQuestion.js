import { LightningElement, api } from 'lwc';
import ASSETS from '@salesforce/resourceUrl/assets';
import SAVE_REFINANCE_PURSE from '@salesforce/apex/CompleteApplication.saveRefinancePurpose';
import GET_REFINANCE_PURSE from '@salesforce/apex/ApplicationService.getRefinancePurpose';

import { showSpinner, hideSpinner, showToast, showNextSlide } from 'c/communityHelper';


export default class CommunityApplicationRefinanceQuestion extends LightningElement {

    @api applicationId;
    @api maxIndexPage;

    refinancePurpose = '';
    svgURL = `${ASSETS}/assets/application/mark.txt#mark`;
    isReadyData = false;

    async connectedCallback(){
        try {
            const response = await GET_REFINANCE_PURSE({ applicationId: this.applicationId });
            this.refinancePurpose = response.refinancePurpose;
            this.isReadyData = true;
            hideSpinner(this);
            console.log('response ', response);
        } catch (err) {
            hideSpinner(this);
            showToast(this, 'Error', 'Something went wrong, please try again.', 'error');
            console.log('err ', err);
        }
    }

    renderedCallback() {
        this.template.querySelectorAll('.item').forEach(item => {
            if (item.dataset.name === this.refinancePurpose) {
                item.classList.add('selected');
            } else {
                item.classList.remove('selected');
            }
        });
    }

    selectItem(event) {
        this.template.querySelectorAll('.item').forEach(item => item.classList.remove('selected'));
        this.refinancePurpose = event.currentTarget.dataset.name;
        event.currentTarget.classList.add('selected');
    }

    saveApplication() {
        console.log(this.refinancePurpose);
        console.log(this.applicationId);
        console.log('this.maxIndexPage ', this.maxIndexPage);

        showSpinner(this);

        SAVE_REFINANCE_PURSE({
            wrapper : {
                refinancePurpose : this.refinancePurpose, 
                applicationId : this.applicationId,
                lastApplicationStage: this.maxIndexPage + '',
            }})
        .then(() => {
            console.log('savie Refinance Purpose data - success ');
            showNextSlide(this);
        })
        .catch((err) => {
            console.log(err);
            showToast(this, 'Error', 'Something went wrong, please try again.', 'error');
        });
    }

}