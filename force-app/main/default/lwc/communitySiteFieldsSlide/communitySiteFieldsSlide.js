import { LightningElement, api } from 'lwc';

export default class CommunitySiteFieldsSlide extends LightningElement {
	@api assets;

	typeOn(e) {
		e.preventDefault();
		this.dispatchEvent(
			new CustomEvent('type', {
				detail: {
					index: e.currentTarget.dataset.index,
					value: e.currentTarget.value,
				},
			}),
		);
	}
}