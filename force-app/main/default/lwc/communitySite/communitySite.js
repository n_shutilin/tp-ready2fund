import { LightningElement, track } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import ASSETS from '@salesforce/resourceUrl/communityStartPageCss';

import GET_EMAIL from '@salesforce/apex/SubmitApplication.getEmail';

export default class CommunitySite extends LightningElement {
	showRegistartion = true;
	showEmailVerify = false;
	isVerifying = false;
	isVerifyingFinish = false;
	IsShowAgreementSlide = false;
	rId = '';
	paId = '';
	isShowSpinner = true;
	headerSlideNine = '';

	connectedCallback() {
		loadStyle(this, `${ASSETS}/community_start_page/typekit.css`);
		loadStyle(this, `${ASSETS}/community_start_page/community_start.css`);
		const url = new URL(window.location.href);
		if (
			url.searchParams.get('recordId') !== null &&
			url.searchParams.get('recordId') !== undefined &&
			url.searchParams.get('partnerAccountId') !== null &&
			url.searchParams.get('partnerAccountId') !== undefined
		) {
			this.isVerifying = true;
			this.showRegistartion = false;
			this.rId = url.searchParams.get('recordId');
			this.paId = url.searchParams.get('partnerAccountId');
		}

		if (this.rId !== '') {
			GET_EMAIL({ wrapper: { recordId: this.rId } })
				.then((res) => {
					this.headerSlideNine = `Your email address (${res}) will be your Username.`;
					setTimeout(() => (this.isShowSpinner = false), 500);
				})
				.catch(() => {
					this.headerSlideNine = 'Your email address (example@example.com) will be your Username.';
					setTimeout(() => (this.isShowSpinner = false), 500);
				});
		} else {
			setTimeout(() => (this.isShowSpinner = false), 500);
		}
	}

	showVerify() {
		this.showRegistartion = false;
		this.showEmailVerify = true;
	}

	goNext() {
		this.isVerifying = false;
		this.showRegistartion = true;
	}
}