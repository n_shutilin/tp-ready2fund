import { LightningElement } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import CSS from '@salesforce/resourceUrl/communityApplicationPageCss';
import SITE_IMGS from '@salesforce/resourceUrl/communitySiteIMG';
import APPLICATION_IMGS from '@salesforce/resourceUrl/communityApplicationImages';

export default class CommunityApplicationContainer extends LightningElement {
	isShowSpinner = true;
	isFirstLoad= true;
	applicationId = '';

	connectedCallback() {
		const url = new URL(window.location.href);
		this.applicationId = url.searchParams.get('applicationId') ? url.searchParams.get('applicationId') : '';

		this.logoURL = `${SITE_IMGS}/logo.png`;
		this.phoneURL = `${SITE_IMGS}/icon-phone.png`;
		this.accountURL = `${APPLICATION_IMGS}/community_application_image/user_account.png`;
		this.talkUsURL = `${APPLICATION_IMGS}/community_application_image/talk_to_us.png`;
		Promise.all([
			loadStyle(this, `${CSS}/community_application_page/typekit.css`),
			loadStyle(this, `${CSS}/community_application_page/community_application.css`),
		]).then(() => {
			setTimeout(() => {
				this.isFirstLoad = false;
				this.isShowSpinner = false;				
			}, 1000);
		});		
	}

	showSpinner() {
		this.isShowSpinner = true;
	}

	hideSpinner() {
		this.isShowSpinner = false;
	}
	
}