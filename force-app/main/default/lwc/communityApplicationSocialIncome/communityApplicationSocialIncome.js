import { LightningElement, api, track } from 'lwc';

const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationSocialIncome extends LightningElement {

    @api index;
    @api socialInfo;
    @track socialIncomeTypes = [
        { name: 'I\'m Self-Employed', value: 'I\'m Self-Employed', isSelected: false },
        { name: 'I Receive Social Security/Disability', value: 'I Receive Social Security/Disability', isSelected: false },
        { name: 'I Receive Pension/Retirement/401(k)', value: 'I Receive Pension/Retirement/401(k)', isSelected: false },
    ];
    yearIncome= '';
    selectedSocialIncome = '';

    connectedCallback() {
        this.socialIncomeTypes.forEach(item => {
            if (item.value === this.socialInfo.socialIncomeType) {
                item.isSelected = true;
                this.selectedSocialIncome = item.value;
            } else {
                item.isSelected = false;
            }
        })
    }

    renderedCallback() {
        this.template.querySelectorAll('.currencyField').forEach(item => {
            item.value = item.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        });
    }

    selectSocialIncomeType(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
    }

    setCurrencyField(event) {            
        let val = event.currentTarget.value.split('.').join('');        
        if (checkNumbers.test(val) || val === '') {
            this[event.currentTarget.dataset.name] = val;
            this.setValue(this.index, event.currentTarget.dataset.name, val);
            event.currentTarget.value = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");            
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    }

    setIncomeType(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.checked);
    }

    setValue(index, name, value) {
        this.dispatchEvent(new CustomEvent('socialincome', {
            detail: { index, data: { [name] : value } },
        }));
    }

    cancel() {
        this.dispatchEvent(new CustomEvent('cancelsocialincome', {
            detail: { index: this.index },
        }));
    }

    switchToEdit() {
        this.dispatchEvent(new CustomEvent('editsocialincome', {
            detail: { index: this.index, target: 'socialIncomes' },
        }));
    }
}