import { LightningElement, api, track } from 'lwc';
import GET_LAST_STAGE from '@salesforce/apex/ApplicationService.getLastApplicationStage';

import { showSpinner, hideSpinner, uniqueId, showToast } from 'c/communityHelper';

const stages = ['Refinance Purpose', 'My Current Mortgage', 'My Home Info', 'Who\'s on the Loan', 'Assets and Income', 'Government Questions', 'My Credit', 'My Solution', 'Upload files'];

export default class CommunityApplicationQuestionnaire extends LightningElement {
    @api applicationId;

    currentPageIndex = 0;
    maxIndexPage = 0;

    @track steps = this.getSteps();

    async connectedCallback() {
        try {
            showSpinner(this);
            const response = await GET_LAST_STAGE({ applicationId: this.applicationId });
            this.currentPageIndex = response.lastApplicationStage;
            //this.maxIndexPage = response.lastApplicationStage;
            this.maxIndexPage = 8;
            console.log('this.currentPageIndex ', this.currentPageIndex);
            console.log('this.maxIndexPage ', this.maxIndexPage);
            this.steps = this.steps.map((item, index) => {
                if (index < this.maxIndexPage && index !== this.currentPageIndex) {
                    return { ...item, className: 'complete', }
                } else if (index === this.currentPageIndex) {
                    return { ...item, className: 'current', }
                } else {
                    return { ...item, className: 'future', }
                }
            });
            this.showSelectedSlide(this.currentPageIndex);
        } catch (err) {
            console.log('error ', err);
            hideSpinner(this);
            showToast(this, 'Error', 'Something went wrong, please try again.', 'error');
        }
    }

    getSteps() {
        return stages.map((item, index) => {
            if (index === 0) {
                return {
                    key: uniqueId(),
                    className: 'current',
                    name: item,
                }
            }
            return {
                key: uniqueId(),
                className: 'future',
                name: item,
            }
        });
    }

    selectStep(event) {
        this.steps = this.steps.map((item, index) => {
            if (index === this.currentPageIndex) {
                return { ...item, className: 'complete' };
            } else if (index === +event.detail.index) {
                return { ...item, className: 'current' };
            }
            return item;
        });
        this.currentPageIndex = +event.detail.index;
        this.showSelectedSlide(this.currentPageIndex);
    }

    showSelectedSlide(pageIndex) {
        this.template.querySelector('c-community-application-questions').selectSlide(pageIndex);
    }

    nextSlide() {
        this.currentPageIndex += 1;
        this.maxIndexPage = this.currentPageIndex > this.maxIndexPage ? this.currentPageIndex : this.maxIndexPage;

        this.steps = this.steps.map((item, index) => {
            if (index < this.currentPageIndex) {
                return { ...item, className: 'complete' };
            } else if (index === this.currentPageIndex) {
                return { ...item, className: 'current' };
            }
            return item;
        });

        this.showSelectedSlide(this.currentPageIndex);

    }
}