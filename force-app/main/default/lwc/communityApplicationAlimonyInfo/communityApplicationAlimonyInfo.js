import { LightningElement, track, api } from 'lwc';

const checkNumbers = new RegExp(/^\d+$/);

export default class CommunityApplicationAlimonyInfo extends LightningElement {

    @api alimonyInfo;
    @api index;
    @track alimonyTypes = [
        { name: 'Alimony', value: 'Alimony', isSelected: false },
        { name: 'Child support', value: 'Child support', isSelected: false },
        { name: 'Separate Maintenance', value: 'Separate Maintenance', isSelected: false },
    ];
    remainingPayments = '';
    monthlyPay = '';
    isNotFirst = false;

    connectedCallback() {
        console.log(JSON.parse(JSON.stringify(this.alimonyInfo)));
        console.log('index ', this.index);
        this.alimonyTypes.forEach(item => {
            if (item.value === this.alimonyInfo.alimonyType) {
                item.isSelected = true;
            } else {
                item.isSelected = false;
            }
        })
        this.isNotFirst = +this.index !== 0;
    }

    selectAlimonyType(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
    }

    setTextField(event) {
        this.setValue(this.index, event.currentTarget.dataset.name, event.currentTarget.value);
    }

    setCurrencyField(event) {            
        let val = event.currentTarget.value.split('.').join('');        
        if (checkNumbers.test(val) || val === '') {
            this[event.currentTarget.dataset.name] = val;
            this.setValue(this.index, event.currentTarget.dataset.name, val);
            event.currentTarget.value = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");            
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
    }

    setNumberField(event) {            
        let val = event.currentTarget.value;        
        if (checkNumbers.test(val) || val === '') {
            this[event.currentTarget.dataset.name] = val;
            this.setValue(this.index, event.currentTarget.dataset.name, val);            
        } else {
            event.currentTarget.value = this[event.currentTarget.dataset.name];
        }
    }

    setValue(index, name, value) {
        this.dispatchEvent(new CustomEvent('newalimony', {
            detail: { index, data: { [name] : value } },
        }));
    }

    cancel() {
        this.dispatchEvent(new CustomEvent('deletealimony', {
            detail: { index: this.index },
        }));
    }
}