import { LightningElement } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import APEX_FORGOT_PASSWORD from '@salesforce/apex/SubmitApplication.forgotPassowrd';
import ASSETS from '@salesforce/resourceUrl/communitySignInCss';

export default class CommunitySiteForgotPassword extends LightningElement {
	username = '';
	isSuccess = false;
	isError = false;
	isShowSpinner = true;

	connectedCallback() {
		loadStyle(this, `${ASSETS}/signin_forgot_pass/typekit.css`);
		loadStyle(this, `${ASSETS}/signin_forgot_pass/signin_forgot.css`);
		setTimeout(() => (this.isShowSpinner = false), 500);
	}

	typeOn(e) {
		this.username = e.currentTarget.value;
	}

	reset() {
		this.isError = false;
		APEX_FORGOT_PASSWORD({ wrapper: { email: this.username.toLowerCase().trim() } })
			.then((result) => {
				if (result === 'LoginResetSuccess') {
					this.isSuccess = true;
				} else {
					this.isError = true;
				}
			})
			.catch(() => {
				this.isError = true;
			});
	}

	cancel(e) {
		e.preventDefault();
		const url = window.location.href;
		const index = url.indexOf('/ForgotPassword');
		window.location.assign(url.slice(0, index));
	}
}