/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * SubmitApplication class for registrate new Customer Community User and insert Application object.
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           04/02/2021                   Created resendEmail method for send VerifyEmail one more time
 * Dmitry Goshko           04/06/2021                   Created forgotPassowrd method for send email with link for refresh password
 * Dmitry Goshko           04/08/2021                   Created checkEmail method for verify exists emails
 * Dmitry Goshko           04/14/2021                   Created submit method for insert application and send verify email
 * Dmitry Goshko           04/18/2021                   Created createSiteUser method for create Customer Community User
 * Dmitry Goshko           04/21/2021                   Created sendSendGridWelcomeMail method for send request to SendGrid
 * Dmitry Goshko           04/22/2021                   Removed Household account from the user-registration logic
**/
global with sharing class SubmitApplication {

    /**
    * Method for send VerifyEmail and WelcomeEmail one more time
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    global static void resendEmail(Map<String, String> wrapper) {
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = getCommunitySettings();
        Map<String, String> templateMap = getTemplateId();
        String apiKey = communitySettings.get('Community_Settings').SendGrid_API_Key__c;
        String sendEmailFrom = communitySettings.get('Community_Settings').Send_Email_From__c;
        Contact preUser = PreUserAccess.getPreUserId(wrapper.get('email'));

        if (preUser != null) {
            String info = PreUserAccess.sendCustomEmail('VerifyEmailAddres', wrapper.get('email'), preUser);
        }
    }

    /**
    * Method for send email with link for refresh password
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @AuraEnabled
    public static String forgotPassowrd(Map<String, String> wrapper) {
      String procesMsg = '';
      if (wrapper.get('email') != null) {
        if (Site.isValidUsername(wrapper.get('email'))) {
          Site.forgotPassword(wrapper.get('email'));
          procesMsg = 'LoginResetSuccess';
        } else {
          procesMsg = 'LoginResetWarning';
        }
      }
      return procesMsg;
    }

    /**
    * Method for verify exists emails
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static boolean checkEmail(Map<String, String> wrapper) {
        Boolean isValid = false;
        if(wrapper.get('email') != null) {
            isValid = PreUserAccess.checkEmail(wrapper.get('email'));
        }

        return isValid;
    }

    /**
    * Method for insert application, borrower and send verify email
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void submit(Map<String, String> wrapper) {
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = getCommunitySettings();
        Application__c app = new Application__c();
        String alias = '';

        app.Type_of_Loan__c = wrapper.get('typeOfLoan');
        app.Home_Description__c = wrapper.get('homeDescription');
        app.Property_Use__c = wrapper.get('propertyUse');
        app.Purchase_Timeframe__c = wrapper.get('purchaseTimeframe');
        app.First_Time_Buyer__c = wrapper.get('firstTimeBuyer');
        app.Credit_Profile__c = wrapper.get('creditProfile');
        app.X2nd_Mortgage__c = wrapper.get('x2andMortgage');
        app.Applicant_Email__c = wrapper.get('email');

        if(wrapper.containsKey('partnerAccountId')) {
            app.Account__c = wrapper.get('partnerAccountId');
        } else {
            app.Account__c = communitySettings.get('Community_Settings').Partner_Account_Id__c;
        }
        
        Database.SaveResult appResult = PreUserAccess.insertApplication(app);

        Borrower__c bor = new Borrower__c();
        bor.Application__c = appResult.getId();
        bor.First_Name__c = wrapper.get('firstName');
        bor.Last_Name__c = wrapper.get('lastName');
        bor.Email__c = wrapper.get('email');
        bor.Type__c = 'Primary';
        Database.SaveResult borResult = PreUserAccess.insertBorrower(bor);

        if(wrapper.get('firstName').length() > 3) {
            alias = wrapper.get('lastName').substring(1, 1) + wrapper.get('firstName').substring(1, 4);
        } else {
            alias = wrapper.get('lastName').substring(1, 1) + wrapper.get('firstName');
        }

        Contact preUser = new Contact();
        preUser.Alias__c = alias;
        preUser.CommunityNickname__c = wrapper.get('firstName');
        preUser.LastName = wrapper.get('lastName');
        preUser.FirstName = wrapper.get('firstName');
        preUser.Username__c = wrapper.get('email');
        preUser.Email = wrapper.get('email');
        preUser.PartnerAccountId__c = communitySettings.get('Community_Settings').Partner_Account_Id__c;

        if(Test.isRunningTest()) {
            preUser.CommunityURL__c = communitySettings.get('Community_Settings').CommunityURL__c;
        } else {
            String ntwrkId = Network.getNetworkId();
            ConnectApi.Community comm = ConnectApi.Communities.getCommunity(ntwrkId);
            preUser.CommunityURL__c = String.valueOf(comm.siteUrl);
        }

        insert preUser;

        String info = PreUserAccess.sendCustomEmail('VerifyEmailAddres', wrapper.get('email'), preUser);
    } 

    /**
    * Method for create Customer Community User, Household account and update Application
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void createSiteUser(Map<String, String> wrapper) {
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = getCommunitySettings();
        String apiKey = communitySettings.get('Community_Settings').SendGrid_API_Key__c;
        String sendEmailFrom = communitySettings.get('Community_Settings').Send_Email_From__c;

        //Create user
        Profile portalProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Ready2Fund Customer Community User'];
        Contact preUser = PreUserAccess.getPreUserInfo(wrapper.get('recordId'));

        if(preUser != null) {
            User portalUser = new User(
            Username = preUser.Username__c,
            ProfileId = portalProfile.Id,
            Alias = preUser.Alias__c,
            Email = preUser.Email,
            EmailEncodingKey = 'UTF-8',
            LastName = preUser.LastName,
            FirstName = preUser.CommunityNickname__c,
            CommunityNickname = preUser.CommunityNickname__c,
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
            );

            Database.DeleteResult result = PreUserAccess.deletePreUser(preUser.Id);

            String userId = Site.createExternalUser(portalUser, wrapper.get('partnerAccountId'), wrapper.get('password'));

            Map<String, String> templateMap = getTemplateId();
            if(!Test.isRunningTest()) {
                sendSendGridWelcomeMail(apiKey, sendEmailFrom, templateMap, portalUser.Email);
            }

            // "Verify email" was send while Customer Community User creation
            // sendSendGridVerifyEmail(communitySettings.get('Community_Settings').SendGrid_API_Key__c, templateMap, wrapper));
        }
    }

    /**
    * Method for getting User email
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static String getEmail(Map<String, String> wrapper) {
        String email = '';
        
        if(PreUserAccess.getPreUserInfo(wrapper.get('recordId')) != null) {
            email = PreUserAccess.getPreUserInfo(wrapper.get('recordId')).Email;
        }
        
        return email;
    }

    /**
    * Method for login to Customer Community
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static String login(Map<String, String> wrapper) {
        PageReference communityPage = Site.login(wrapper.get('username'), wrapper.get('password'), wrapper.get('startUrl'));
        return communityPage != null ? communityPage.getURL() : null;
    }

    /**
    * Method for get template information from the custom metadata type SendGrid_Templates__mdt
    *
    * @author Dmitry Goshko
    *
    * @return Map<String, String> templateMap - Map with metadata type info
    */
    public static Map<String, String> getTemplateId() {
        Map<String, String> templateMap = new Map<String, String>();

        for(SendGrid_Templates__mdt template : [SELECT MasterLabel, DeveloperName, TemplateId__c 
                                                FROM SendGrid_Templates__mdt]) {
            templateMap.put(template.DeveloperName, template.TemplateId__c);
        }

        return templateMap;
    }

    /**
    * Method for get community information from the custom metadata type Ready2Fund_Community_Seetings__mdt
    *
    * @author Dmitry Goshko
    *
    * @return Map<String, Ready2Fund_Community_Seetings__mdt> settingMap - Map with metadata type info
    */
    public static Map<String, Ready2Fund_Community_Seetings__mdt> getCommunitySettings() {
        Map<String, Ready2Fund_Community_Seetings__mdt> settingMap = new Map<String, Ready2Fund_Community_Seetings__mdt>();

        for(Ready2Fund_Community_Seetings__mdt setting : [SELECT CommunityURL__c, Partner_Account_Id__c, MasterLabel, DeveloperName, SendGrid_API_Key__c, Send_Email_From__c
                                                          FROM Ready2Fund_Community_Seetings__mdt]) {
            settingMap.put(setting.DeveloperName, setting);
        }

        return settingMap;
    }

    /**
    * Method for send request to SendGrid
    *
    * @author Dmitry Goshko
    *
    * @param String apiKey - API key for header
    * @param String sendEmailFrom - Email for FROM setting
    * @param Map<String, String> templateMap - Map with SendGrid templates information
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @future(callout=true)
    public static void sendSendGridWelcomeMail(String apiKey, String sendEmailFrom, Map<String, String> templateMap, String email) {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://api.sendgrid.com/v3/mail/send');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + apiKey);
        request.setBody('{'+
                        + '"from":{'+
                            + '"email":"' + sendEmailFrom + '"'+
                            + '},'+
                            + '"personalizations":['+
                                + '{'+
                                    + '"to":['+
                                        +'{'+
                                        +'"email":"' + email + '"'+
                                        +'}'+
                                    +']'+
                                +'}'+
                            +'],'+
                            +'"template_id":"' + templateMap.get('WelcomeMail') + '"'+
                            +'}');
        System.debug('request: ' + request.getHeader('Authorization'));
        System.debug('request: ' + request.getBody());
        HttpResponse response = http.send(request);
        System.debug('response: ' + response);
        // Parse the JSON response
        if (response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' +
                response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
    }    

    /**
    * Method for send request to SendGrid
    *
    * @author Dmitry Goshko
    *
    * @param String apiKey - API key for header
    * @param String sendEmailFrom - Email for FROM setting
    * @param Map<String, String> templateMap - Map with SendGrid templates information
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    /*@future(callout=true)
    public static void sendSendGridVerifyEmail(String apiKey, String sendEmailFrom, Map<String, String> templateMap, Map<String, String> wrapper) {
        String verufyUrl = '';

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://api.sendgrid.com/v3/mail/send');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + apiKey);
        request.setBody('{'+
                        + '"from":{'+
                            + '"email":"' + sendEmailFrom + '"'+
                            + '},'+
                            + '"personalizations":['+
                                + '{'+
                                    + '"to":['+
                                        +'{'+
                                        +'"email":"' + wrapper.get('email') + '"'+
                                        +'}'+
                                    +'],'+
                                    +'"dynamic_template_data":{'+
                                        +'"first_name":"' + wrapper.get('firstName') + '",'+
                                        +'"verify_email": "' + verufyUrl + '"'+
                                    +'}'+
                                +'}'+
                            +'],'+
                            +'"template_id":"' + templateMap.get('VerifyEmail') + '"'+
                            +'}');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' +
                response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
    }*/
}