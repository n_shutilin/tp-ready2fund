/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * CompleteApplication class for continue fill Application record.
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           04/23/2021                   Created method for Refinance Purpose slide
 * Dmitry Goshko           04/23/2021                   Created method for My Current Mortgage slide
 * Dmitry Goshko           04/23/2021                   Created method for Home Info slide
 * Dmitry Goshko           04/24/2021                   Created method for Who's on the Loan slide
 * Dmitry Goshko           04/24/2021                   Created method for My Credit slide
 * Dmitry Goshko           04/25/2021                   Created method for Income slide
 * Dmitry Goshko           04/25/2021                   Created method for add another job on Income slide
 * Dmitry Goshko           04/29/2021                   Created method for Government Questions slide
 * Dmitry Goshko           05/05/2021                   Created method for get Home Info
 * Dmitry Goshko           05/12/2021                   Created method for save Alimony Payment on Government Question slide
 * Dmitry Goshko           05/17/2021                   Created method for get information on Refinance Solution slide
 * Dmitry Goshko           05/25/2021                   Created method for Upload files
**/
public with sharing class CompleteApplication {

    /**
    * Method for save and continue Application on Refinance Purpose slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveRefinancePurpose(Map<String, String> wrapper) {
        Application__c app = new Application__c();

        app.id = wrapper.get('applicationId');
        app.Refinance_Purpose__c = wrapper.get('refinancePurpose');
        app.Application_Stage__c = 'My Current Mortgage';

        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 1) {
            app.Last_Application_Stage__c = 1;
        }
        

        update app;
    }

    /**
    * Method for save and continue REO on My Current Mortgage slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveMyCurrentMortgage(Map<String, String> wrapper) {
        Application__c app = new Application__c();
        REO__c reo = new REO__c();
        Borrower__c bor = new Borrower__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'My Home Info';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 2) {
            app.Last_Application_Stage__c = 2;
        }

        upsert app;

        bor.Id = wrapper.get('borrowerId');
        bor.Application__c = wrapper.get('applicationId');
        bor.Military_or_Surviving_Spouse__c = wrapper.get('militarySurvivingSpouse');
        update bor;

        reo.Application__c = wrapper.get('applicationId');
        reo.Mortgage_Payment__c = Decimal.valueOf(wrapper.get('mortgagePayment'));
        reo.Pmt_Includes_P_I__c	= Boolean.valueOf(wrapper.get('pmtIncludesPI'));
        reo.Mortgage_Lien__c = Decimal.valueOf(wrapper.get('mortgageLien'));
        reo.Market_Value__c = Decimal.valueOf(wrapper.get('marketValue'));
        insert reo;
    }

    /**
    * Method for save and continue Application on My Home Info slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveHomeInfoFirstPart(Map<String, String> wrapper) {
        Application__c app = new Application__c();
        REO__c reo = new REO__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'My Home Info';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 2) {
            app.Last_Application_Stage__c = 2;
        }

        upsert app;

        reo.id = wrapper.get('reoId');
        reo.Application__c = wrapper.get('applicationId');
        reo.Address__c = wrapper.get('address');
        reo.Address_2__c = wrapper.get('address2');
        reo.City__c = wrapper.get('city');
        reo.State__c = wrapper.get('state');
        reo.Postal_Code__c = wrapper.get('postalCode');

        upsert reo;
    }

    /**
    * Method for get Home Info
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static Map<String, String> getHomeInfo(Map<String, String> wrapper) {
        Map<String, String> reoInfoMap = new Map<String, String>();

        for(REO__c reoItem : [SELECT Id, Name, Address__c, Property_Type__c, Occupancy__c FROM REO__c WHERE Id =: wrapper.get('reoId')]) {
            reoInfoMap.put('address', reoItem.Address__c);
            reoInfoMap.put('propertyType', reoItem.Property_Type__c);
            reoInfoMap.put('occupancy', reoItem.Occupancy__c);
        }

        return reoInfoMap;
    }

    /**
    * Method for save and continue Application on My Home Info slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveHomeInfoSecondPart(Map<String, String> wrapper) {
        Application__c app = new Application__c();
        REO__c reo = new REO__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'Who\'s on the Loan';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 3) {
            app.Last_Application_Stage__c = 3;
        }

        upsert app;

        reo.Id = wrapper.get('reoId');
        reo.Property_Type__c = wrapper.get('oropertyType');
        reo.Occupancy__c = wrapper.get('occupancy');
        reo.Operate_Business_on_Property__c = wrapper.get('operateBusinessOnProperty');
        reo.Tax_Expense__c = Decimal.valueOf(wrapper.get('taxExpense'));
        reo.Insurance_Provider__c = wrapper.get('insuranceProvider');
        reo.Insurance_Expense__c = Decimal.valueOf(wrapper.get('insuranceExpense'));
        reo.Original_Purchase_Prices__c = Decimal.valueOf(wrapper.get('originalPurchasePrices'));
        reo.Year_Built__c = Decimal.valueOf(wrapper.get('yearBuilt'));
        reo.Started_Living_Here_Month__c = Decimal.valueOf(wrapper.get('startedLivingHereMonth'));
        reo.Started_Living_Here_Year__c = Decimal.valueOf(wrapper.get('startedLivingHereYear'));

        upsert reo;
    }

    /**
    * Method for save and continue Application on Who's on the Loan slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveWhosOnTheLoan(Map<String, String> wrapper) {
        Borrower__c bor = new Borrower__c();
        Application__c app = new Application__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'Assets and Income';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 4) {
            app.Last_Application_Stage__c = 4;
        }

        upsert app;

        bor.Application__c = wrapper.get('applicationId');
        bor.Id = wrapper.get('borrowerId');
        bor.Marital_Status__c = wrapper.get('maritalStatus');
        bor.First_Name__c = wrapper.get('firstName');
        bor.Type__c = 'Spouse';
        bor.Last_Name__c = wrapper.get('lastName');
        bor.Email__c = wrapper.get('email');
        bor.Spouse_Military_or_Surviving_Spouse__c = wrapper.get('militaryOrSurviving');

        upsert bor;
    }

    /**
    * Method for save and continue Application on Income slide
    *
    * @author Dmitry Goshko
    *
    * @param List<Map<String, String>> wrapper - List of Maps with needed values comes from LWC
    * @param String applicationId - id of selected Application record
    * @param String borrowerId - id of Borrower object
    * @param Boolean isCurrent - flag for current and prev. job
    */
    @Auraenabled
    public static void saveIncome(List<Map<String, String>> wrapper, String applicationId, String borrowerId, Boolean isCurrent, String lastApplicationStage) {
        Application__c app = new Application__c();

        app.id = applicationId;
        app.Application_Stage__c = 'Government Questions';
        
        if(Decimal.valueof(lastApplicationStage) < 5) {
            app.Last_Application_Stage__c = 5;
        }

        upsert app;

        CompleteApplication.addAnotherJob(wrapper, borrowerId, isCurrent);
    }

    /**
    * Method for save Additional Income on Income slide
    *
    * @author Dmitry Goshko
    *
    * @param List<Map<String, String>> wrapper - List of Maps with needed values comes from LWC
    * @param String borrowerId - id of Borrower object
    */
    @Auraenabled
    public static void saveAdditionalIncome(List<Map<String, String>> wrapper, String borrowerId) {
        List<Additional_Income__c> addIncomeList = new List<Additional_Income__c>();

        for(Map<String, String> item : wrapper) {
            Additional_Income__c addIncome = new Additional_Income__c();
            addIncome.Borrower__c = borrowerId;
            addIncome.Non_Employer_Income_Source__c = item.get('socialIncomeType');
            addIncome.Non_Employer_Yearly_Income__c = Decimal.valueOf(item.get('yearIncome'));
            addIncomeList.add(addIncome);
        }

        insert addIncomeList;
    }

    /**
    * Method for add Another Job on Income slide
    *
    * @author Dmitry Goshko
    *
    * @param List<Map<String, String>> wrapper - List of Maps with needed values comes from LWC
    * @param String borrowerId - id of Borrower object
    * @param Boolean isCurrent - flag for current and prev. job
    */
    @Auraenabled
    public static void addAnotherJob(List<Map<String, String>> wrapper, String borrowerId, Boolean isCurrent) {
        List<Employment__c> empList = new List<Employment__c>();

        for(Map<String, String> item : wrapper) {

            Employment__c emp = new Employment__c();
            emp.Borrower__c = borrowerId;

            if(isCurrent) {
                emp.Base_Yearly_Income__c = Decimal.valueOf(item.get('baseIncome'));
                emp.Employment_Start_Month__c = Decimal.valueOf(item.get('startMonth'));
                emp.Employed_by_Family_Member__c = item.get('isEmployerFamilyMember');
                emp.Employment_Start_Year__c = Decimal.valueOf(item.get('startYear'));
                //emp.Bonus_Income__c = Decimal.valueOf(item.get('bonus'));
                //emp.Commission__c = Decimal.valueOf(item.get('commission'));
                //emp.Overtime__c = Decimal.valueOf(item.get('overtime'));
                emp.Employment_Type__c = 'Current';
                emp.Employer_Name__c = item.get('employerName');
                emp.Borrower_Type__c = 'Primary';
                emp.Employer_Phone__c = item.get('employerPhoneOne') + item.get('employerPhoneTwo') + item.get('employerPhoneThree');
                emp.Job_Title__c = item.get('jobTitle');
                emp.Job_Type__c = item.get('jobType');         
            } else {
                emp.Employment_Start_Month__c = Decimal.valueOf(item.get('startMonth'));
                emp.Employment_Start_Year__c = Decimal.valueOf(item.get('startYear'));
                emp.Employment_End_Month__c = Decimal.valueOf(item.get('endMonth'));
                emp.Employment_End_Year__c = Decimal.valueOf(item.get('endYear'));
                emp.Employment_Type__c = 'Previous';
                emp.Employer_Name__c = item.get('employerName');
                emp.Borrower_Type__c = 'Primary';
            }
            empList.add(emp);
        }

        insert empList; 
    }

    /**
    * Method for save and continue Application on Government Question slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveGovernmentQuestion(Map<String, String> wrapper) {
        Declaration__c dec = new Declaration__c();
        Borrower__c bor = new Borrower__c();
        Application__c app = new Application__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'My Credit';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 6) {
            app.Last_Application_Stage__c = 6;
        }

        upsert app;

        bor.id = wrapper.get('borrowerId');
        bor.Race__c = wrapper.get('raceValue');
        bor.Race_Other__c = wrapper.get('otherRaceValue');
        bor.Ethnicity__c = wrapper.get('ethnicitValue');
        bor.Ethnicity_Other__c = wrapper.get('otherEthnicitValue');
        bor.Gender__c = wrapper.get('genderValue');

        update bor;

        dec.Borrower__c = wrapper.get('borrowerId');
        dec.Application__c = wrapper.get('applicationId');
        dec.Outstanding_Judgments__c = wrapper.get('outstandingJudgments');
        dec.Bankruptcy__c = wrapper.get('bankruptcy');
        dec.Last_7_Years_Home_Issue__c = wrapper.get('last7YearsHomeIssue');
        dec.Involved_in_Lawsuit__c = wrapper.get('involvedInLawsuit');
        dec.Currently_Delinquent__c = wrapper.get('currentlyDelinquent');
        dec.Borrowed_Funds__c = wrapper.get('borrowedFunds');
        dec.Co_Maker_on_Note__c = wrapper.get('coMakerOnNote');
        dec.U_S_Citizen__c = wrapper.get('usCitizen');
        dec.Required_Alimony__c = wrapper.get('requiredAlimony');
        dec.Foreclosure__c = dec.Last_7_Years_Home_Issue__c == 'Foreclosure' ? 'Yes' : 'No';

        dec.Primary_Residence__c = wrapper.get('isPrimaryResidence');
        dec.Another_Mortgage__c = wrapper.get('isMortgageDifferentProperty');
        dec.New_Line_of_Credit__c = wrapper.get('isOpenNewLine');
        dec.Ownership_Last_3_Years__c = wrapper.get('isOwnershipLast3Years');

        insert dec;
    }

    /**
    * Method for save Alimony Payment on Government Question slide
    *
    * @author Dmitry Goshko
    *
    * @param List<Map<String, String>> wrapper - List of Maps with needed values comes from LWC
    * @param String applicationId - id of selected Application record
    */
    @Auraenabled
    public static void saveAlimonyPayment(List<Map<String, String>> wrapper, String applicationId) {
        Declaration__c dec = new Declaration__c();
        List<Alimony__c> alimonyList = new List<Alimony__c>();

        for(Map<String, String> almn : wrapper) {
            Alimony__c alimony = new Alimony__c();
            alimony.Application__c = applicationId;
            //alimony.Alimony_Type__c = almn.get('alimonyType');
            alimony.Monthly_Alimony_Amount__c = Decimal.valueOf(almn.get('monthlyPay'));
            alimony.Remaining_Monthly_Pmts__c = Decimal.valueOf(almn.get('remainingPayments'));
            alimony.Pmt_Made_To__c = almn.get('paymentReceiver');
            alimonyList.add(alimony);
        }
        
        insert alimonyList;
    }

    /**
    * Method for save Alimony Payment on Government Question slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    * @param String applicationId - id of Application object
    */
    @Auraenabled
    public static void saveWhoHelpingOnTheLoan(List<Map<String, String>> wrapper, String applicationId) {
        List<Loan_Participant__c> participantList = new List<Loan_Participant__c>();

        for(Map<String, String> loan : wrapper) {
            Loan_Participant__c loanPart = new Loan_Participant__c();
            loanPart.Application__c = applicationId;
            loanPart.Type__c = loan.get('type');
            loanPart.Company__c = loan.get('company');
            loanPart.First_Name__c = loan.get('firstName');
            loanPart.Last_Name__c = loan.get('lastName');
            loanPart.Email__c = loan.get('email');
            loanPart.Mobile__c = loan.get('mobile');
            participantList.add(loanPart);
        }

        insert participantList;
    }

    /**
    * Method for save and continue Application on My Credit slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static void saveMyCredit(Map<String, String> wrapper) {
        Borrower__c bor = new Borrower__c();
        Application__c app = new Application__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'My Solution';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 7) {
            app.Last_Application_Stage__c = 7;
        }

        upsert app;

        bor.id = wrapper.get('borrowerId');
        bor.Date_of_Birth__c = Date.parse(wrapper.get('dateOfBirth'));
        bor.SSN__c = wrapper.get('ssn');
        bor.Mobile__c = wrapper.get('mobile');

        update bor;
    }


    /**
    * Method for save Alimony Payment on Government Question slide
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    public static Map<String, String> getRefinanceSolutionInfo(Map<String, String> wrapper) {
        Map<String, String> optionMap = new Map<String, String>();
        Application__c app = new Application__c();

        app.id = wrapper.get('applicationId');
        app.Application_Stage__c = 'Upload files';
        
        if(Decimal.valueof(wrapper.get('lastApplicationStage')) < 8) {
            app.Last_Application_Stage__c = 8;
        }

        upsert app;

        for(Credit_Score__c score : [SELECT Id, Application__c, Credit_Score__c, Borrower__c, Borrower_Name__c, Borrower_Type__c
                                     FROM Credit_Score__c 
                                     WHERE Application__c =: wrapper.get('applicationId')]) {
            if(score.Borrower_Type__c == 'Primary') {
                optionMap.put('creditScore', String.valueOf(score.Credit_Score__c));
                optionMap.put('userName', String.valueOf(score.Borrower_Name__c));
            } else if(score.Borrower_Type__c == 'Spouse') {
                optionMap.put('spouseCreditScore', String.valueOf(score.Credit_Score__c));
                optionMap.put('spouseName', String.valueOf(score.Borrower_Name__c));
                optionMap.put('isSpouse', 'true');
            }
        }

        for(Loan_Option__c loan : [SELECT Id, Application__c, Term__c, Current_Monthly_Payment__c, New_Monthly_Payment__c,
                                   Current_Monthly_Difference__c, Fixed_Rate_APR__c, Estimated_Closing_Costs__c, Cash_Due_at_Closing__c,
                                   New_Loan_Amount__c, Interest_Rate_Lock_Period__c, Funds_Needed_in_Reserve__c, Loan_to_Value_Ratio__c,
                                   Points__c, Estimated_Taxes__c, Estimated_Homeowners_Insurance__c, Mortgage_Insurance__c
                                   FROM Loan_Option__c WHERE Application__c =: wrapper.get('applicationId') limit 1]) {
            optionMap.put('loanOptionId', loan.Id);
            optionMap.put('term', loan.Term__c);
            optionMap.put('currentMonthlyPayment', String.valueOf(loan.Current_Monthly_Payment__c));
            optionMap.put('newMonthlyPayment', String.valueOf(loan.New_Monthly_Payment__c));
            optionMap.put('currentMonthlyDifference', String.valueOf(loan.Current_Monthly_Difference__c));
            optionMap.put('fixedRateAPR', String.valueOf(loan.Fixed_Rate_APR__c));
            optionMap.put('estimatedClosingCosts', String.valueOf(loan.Estimated_Closing_Costs__c));
            optionMap.put('cashDueatClosing', String.valueOf(loan.Cash_Due_at_Closing__c));
            optionMap.put('newLoanAmount', String.valueOf(loan.New_Loan_Amount__c));
            optionMap.put('interestRateLockPeriod', String.valueOf(loan.Interest_Rate_Lock_Period__c));
            optionMap.put('fundsNeededinReserve', String.valueOf(loan.Funds_Needed_in_Reserve__c));
            optionMap.put('loantoValueRatio', String.valueOf(loan.Loan_to_Value_Ratio__c));
            optionMap.put('points', String.valueOf(loan.Points__c));
            optionMap.put('estimatedTaxes', String.valueOf(loan.Estimated_Taxes__c));
            optionMap.put('estimatedHomeownersInsurance', String.valueOf(loan.Estimated_Homeowners_Insurance__c));
            optionMap.put('mortgageInsurance', String.valueOf(loan.Mortgage_Insurance__c));
        }

        for(Lien__c lien : [SELECT Id, Est_Interest_Amt__c, Amount__c FROM Lien__c WHERE Application__c =: wrapper.get('applicationId') limit 1]) {
            optionMap.put('currentLoanAmount', String.valueOf(lien.Amount__c));
            optionMap.put('estimatedAmount', String.valueOf(lien.Est_Interest_Amt__c));
        }

        for(Closing_Costs__c lien : [SELECT Id, Amount__c, Type__c, Loan_Option__c FROM Closing_Costs__c WHERE Loan_Option__c =: optionMap.get('loanOptionId') limit 1]) {
            
        }
        
        return optionMap;
    }

    /**
    * Method for save all Files related to application
    *
    * @author Dmitry Goshko
    *
    * @param List<Map<String, Map<String, String>>> wrapper - Map with needed values comes from LWC
    * @param String applicationId - id of Application object
    */
    @Auraenabled
    public static void uploadFiles(List<Map<String, String>> wrapper, String appliactionId, String lastApplicationStage) {
        Map<String, Application_File__c> appFileMap = new Map<String, Application_File__c>();
        Map<String, String> contentVersionMap = new Map<String, String>();
        Set<String> allFileIds = new Set<String>();
        Map<String, List<ContentVersion>> conVerMap = new Map<String, List<ContentVersion>>();
        List<ContentDocumentLink> conDocLinkList = new List<ContentDocumentLink>();
        List<ContentDocumentLink> insertContentDocumentLinkList = new List<ContentDocumentLink>();
        Application__c app = new Application__c();
        appliactionId = 'a005Y00002IMwGHQA1';

        app.id = appliactionId;
        app.Application_Stage__c = 'Upload files';
        
        if(Decimal.valueof(lastApplicationStage) < 8) {
            app.Last_Application_Stage__c = 8;
        }

        upsert app;

        for(Application_File__c files : [SELECT Id, Application__c, Type__c FROM Application_File__c WHERE Application__c =: appliactionId]) {
            appFileMap.put(files.Type__c, files);
        }

        for(Map<String, String> item : wrapper) {
            
            if(!appFileMap.containsKey(item.get('type'))) {
                Application_File__c appFile = new Application_File__c();
                appFile.Type__c = item.get('type');
                appFile.Application__c = appliactionId;
                appFileMap.put(appFile.Type__c, appFile);
            }
            
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S';
            conVer.PathOnClient = item.get('fileName');
            conVer.Title = item.get('fileName');
            conVer.VersionData = EncodingUtil.base64Decode(item.get('base64'));
            
            List<ContentVersion> conVerList = new List<ContentVersion>();
            if(conVerMap.containsKey(item.get('type'))) {
                conVerList = conVerMap.get(item.get('type'));
            }
            conVerList.add(conVer);
            conVerMap.put(item.get('type'), conVerList);            
        }

        if(!conVerMap.isEmpty()) {
            List<ContentVersion> conVerList = new List<ContentVersion>();
            for(List<ContentVersion> conVer : conVerMap.values()) {
                conVerList.addAll(conVer);
            }

            insert conVerList;

            for(String type : conVerMap.keySet()) {
                for(ContentVersion conVer : conVerMap.get(type)) {
                    allFileIds.add(conVer.id);
                    contentVersionMap.put(conVer.id, type);
                }
            }
        }

        if(!appFileMap.isEmpty()) {
            upsert appFileMap.values();
        }

        for(ContentVersion conDoc : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN: allFileIds]) {
            //create ContentDocumentLink  record 
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = appFileMap.get(contentVersionMap.get(conDoc.Id)).Id;
            conDocLink.ContentDocumentId = conDoc.ContentDocumentId;
            conDocLink.shareType = 'V';
            insertContentDocumentLinkList.add(conDocLink);
        }

        insert insertContentDocumentLinkList;
    }
}