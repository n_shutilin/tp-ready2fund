/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * SubmitApplicationTest test class for SubmitApplication and PreUserAccess classes
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           04/21/2021                   Created init setup method for insert test data
 * Dmitry Goshko           04/21/2021                   Created checkEmail_TestMethod test method for cover checkEmail method
 * Dmitry Goshko           04/21/2021                   Created submit_TestMethod test method for cover submit method
 * Dmitry Goshko           04/21/2021                   Created resendEmail_TestMethod test method for cover resendEmail method
 * Dmitry Goshko           04/21/2021                   Created forgotPassowrd_TestMethod test method for cover forgotPassowrd method
 * Dmitry Goshko           04/21/2021                   Created createSiteUser_TestMethod test method for cover createSiteUser method
**/
@IsTest public with sharing class SubmitApplicationTest {

    
    @testSetup static void init() {
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = SubmitApplication.getCommunitySettings();
        Application__c app = new Application__c();
        app.Type_of_Loan__c = 'Home Refinance';
        app.Home_Description__c = 'Condominium';
        app.Property_Use__c = 'Secondary Home';
        app.Purchase_Timeframe__c = 'Immediate';
        app.First_Time_Buyer__c = 'Yes';
        app.Credit_Profile__c = 'Good (660-719)';
        app.X2nd_Mortgage__c = 'Yes';

        insert app;

        Borrower__c bor = new Borrower__c();
        bor.Application__c = app.id;
        bor.First_Name__c = 'Alex';
        bor.Last_Name__c = 'Kole';
        bor.Email__c = 'secondtest@mail.ru';
        bor.Type__c = 'Primary';
        insert bor;

        Contact preUser = new Contact();
        preUser.Alias__c = 'Alex';
        preUser.CommunityNickname__c = 'Alex';
        preUser.LastName = 'Kole';
        preUser.Username__c = 'secondtest@mail.ru';
        preUser.Email = 'secondtest@mail.ru';
        preUser.CommunityURL__c = communitySettings.get('Community_Settings').CommunityURL__c;
        preUser.PartnerAccountId__c = communitySettings.get('Community_Settings').Partner_Account_Id__c;

        insert preUser;

        Profile portalProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Ready2Fund Customer Community User'];
        User portalUser = new User(
            Username = 'testuser@test.com',
            ProfileId = portalProfile.Id,
            Alias = 'test',
            Email = 'testuser@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'test',
            CommunityNickname = 'test',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
            );

        String userId = Site.createExternalUser(portalUser, communitySettings.get('Community_Settings').Partner_Account_Id__c, 'WorkHome2021');
    }

    @IsTest static void checkEmail_TestMethod() {
        Map<String, String> wrapper = new Map<String, String>();
        Boolean isValid;
        wrapper.put('email', 'test@test.com');

        Test.startTest();
        isValid = SubmitApplication.checkEmail(wrapper);
        Test.stopTest();
    }

    @IsTest static void submit_TestMethod() {
        Map<String, String> wrapper = new Map<String, String>();
        wrapper.put('typeOfLoan', 'Home Refinance');
        wrapper.put('homeDescription', 'Condominium');
        wrapper.put('propertyUse', 'Secondary Home');
        wrapper.put('purchaseTimeframe', 'Immediate');
        wrapper.put('firstTimeBuyer', 'Yes');
        wrapper.put('creditProfile', 'Good (660-719)');
        wrapper.put('x2andMortgage', 'Yes');

        wrapper.put('firstName', 'Alex');
        wrapper.put('lastName', 'Kent');
        wrapper.put('email', 'test@test.com');

        Test.startTest();
        SubmitApplication.submit(wrapper);
        Test.stopTest();
    }

    @IsTest static void resendEmail_TestMethod() {
        Map<String, String> wrapper = new Map<String, String>();
        wrapper.put('email', 'secondtest@mail.ru');

        Test.startTest();
        SubmitApplication.resendEmail(wrapper);
        Test.stopTest();
    }

    @IsTest static void forgotPassowrd_TestMethod() {
        Map<String, String> wrapper = new Map<String, String>();
        wrapper.put('email', 'secondtest@mail.ru');

        Test.startTest();
        SubmitApplication.forgotPassowrd(wrapper);
        Test.stopTest();
    }

    @IsTest static void login_TestMethod() {
        Map<String, String> wrapper = new Map<String, String>();
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = SubmitApplication.getCommunitySettings();

        wrapper.put('username', 'testuser@test.com');
        wrapper.put('password', 'WorkHome2021');
        wrapper.put('startUrl', communitySettings.get('Community_Settings').CommunityURL__c + '/s/login');

        Test.startTest();
        SubmitApplication.login(wrapper);
        Test.stopTest();
    }

    @IsTest static void createSiteUser_TestMethod() {
        Map<String, String> wrapper = new Map<String, String>();
        List<Application__c> appList = new List<Application__c>();
        List<Contact> contactList = new List<Contact>();
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = SubmitApplication.getCommunitySettings();
        String partnerAccountId = communitySettings.get('Community_Settings').Partner_Account_Id__c;

        appList = [SELECT Id FROM Application__c LIMIT 1];
        contactList = [SELECT Id, Username__c FROM Contact WHERE Username__c = 'secondtest@mail.ru' LIMIT 1];

        wrapper.put('recordId', String.valueOf(contactList.get(0).id));
        wrapper.put('partnerAccountId', partnerAccountId);
        wrapper.put('password', 'Workhome2020');
        wrapper.put('applicationId', String.valueOf(appList.get(0).id));

        Test.startTest();
        SubmitApplication.createSiteUser(wrapper);
        Test.stopTest();
    }

    @IsTest static void sendSendGridWelcomeMail_TestMethod() {
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = SubmitApplication.getCommunitySettings();
        String apiKey = communitySettings.get('Community_Settings').SendGrid_API_Key__c;
        String sendEmailFrom = communitySettings.get('Community_Settings').Send_Email_From__c;
        Map<String, String> templateMap = SubmitApplication.getTemplateId();
        Map<String, String> wrapper = new Map<String, String>();

        wrapper.put('email', 'secondtest@mail.ru');

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        Test.startTest();
        SubmitApplication.sendSendGridWelcomeMail(apiKey, sendEmailFrom, templateMap, wrapper);
        Test.stopTest();
    }
}