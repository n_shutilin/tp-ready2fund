/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Ready2FundCommunityHeader class with custom logic for community
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           04/22/2021                   Created resendEmail method for send VerifyEmail one more time
**/
global with sharing class Ready2FundCommunityHeader {

    /**
    * Method for select application and continue populate
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    global static String selectApplication(Map<String, String> wrapper) {
        String applicationInfo = '';
        String selectFields = '';
        Application__c app = new Application__c();
        List<String> applicationFields = getFields('Application__c');
        
        for(String field : applicationFields) {
            selectFields = selectFields + field + ', ';
        }

        selectFields = selectFields.removeEnd(', ');
        app = Database.query('Select ' + selectFields + ' FROM Application__c WHERE Id = ' + wrapper.get('applicationId'));

        applicationInfo = JSON.serialize(app);

        return applicationInfo;
    }

    /**
    * Method for change User email
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    global static void changeEmail(Map<String, String> wrapper) {

    }

    public static List<string> getFields(String selectedObject){
        List<String> reqFields = new List<String>();
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(selectedObject).getDescribe().fields.getMap();
        for(Schema.SObjectField sfield : fieldMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            reqFields.add(dfield.getname());
        }

        System.debug(reqFields);
        return reqFields;
    }
}