/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * PreUserAccess class for get information and update records from Guest user
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           04/02/2021                   Created getPreUserInfo method for get data from Contact
 * Dmitry Goshko           04/06/2021                   Created getPreUserId method for get information from Contact
 * Dmitry Goshko           04/14/2021                   Created checkEmail method for verify exist Contact with current email
 * Dmitry Goshko           04/18/2021                   Created sendCustomEmail method for send "verify email" as guest user
**/
global without sharing class PreUserAccess {

    /**
    * Method for get information from Contact
    *
    * @author Dmitry Goshko
    *
    * @param String recordId - Id of Contact record
    */
    public static Contact getPreUserInfo(String recordId) {
        List<Contact> userList = new List<Contact>();

        if(recordId != null) {
            userList = [SELECT Id, Alias__c, CommunityNickname__c, LastName, Username__c, Email FROM Contact WHERE Id =: recordId];
            if(!userList.isEmpty()) {
                return userList[0];
            }
        }
        return null;
    }

    /**
    * Method for get information from Contact
    *
    * @author Dmitry Goshko
    *
    * @param String email - String with user email
    */
    public static Contact getPreUserId(String email) {
        List<Contact> userList = new List<Contact>();

        if(email != null) {
            userList = [SELECT Id, Alias__c, CommunityNickname__c, LastName, Username__c, Email FROM Contact WHERE Email =: email];
            if(!userList.isEmpty()) {
                return userList[0];
            }
        }
        return null;
    }

    /**
    * Method for verify exist Contact with current email
    *
    * @author Dmitry Goshko
    *
    * @param String email - String with new Email
    */
    public static boolean checkEmail(String email) {
        List<Contact> contactList = new List<Contact>();

        contactList = [SELECT Id, Email FROM Contact WHERE Email =: email];
        if(contactList.isEmpty()) {
            return true;
        }

        return false;
    }

    /**
    * Method for send VerifyEmail and WelcomeEmail one more time
    *
    * @author Dmitry Goshko
    *
    * @param String emailTemplateName - Email Template with VerifyEmail link
    * @param String toAddress - String with email for replyTo
    * @param Contact userInfo - Contact record with preUser information
    */
    public static String sendCustomEmail(String emailTemplateName, String toAddresses, Contact userInfo) {
        String info = '';
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setTargetObjectId(userInfo.id); 
        message.setSenderDisplayName('Ready2Fund'); 
        message.setReplyTo(toAddresses);
        message.setUseSignature(false); 
        message.setBccSender(false); 
        message.setSaveAsActivity(false);

        EmailTemplate emailTemplate = [SELECT Id, Subject, Description, HtmlValue, DeveloperName, Body 
                                    FROM EmailTemplate 
                                    WHERE Name =: emailTemplateName];
        if(emailTemplate != null) {
            message.setTemplateID(emailTemplate.Id);
        }
        
        message.toAddresses = new String[] { toAddresses };
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('The email was sent successfully.');
            info = 'The email was sent successfully.';
        } else {
            System.debug('The email failed to send: ' +  results[0].errors[0].message);
            info = 'The email failed to send: ' +  results[0].errors[0].message;
        }

        return info;
    }

    /**
    * Method for delete Contact
    *
    * @author Dmitry Goshko
    *
    * @param String recordId - Contact id
    */
    public static Database.DeleteResult deletePreUser(String recordId) {
        return Database.delete(recordId);
    }

    /**
    * Method for insert Borrower
    *
    * @author Dmitry Goshko
    *
    * @param Borrower item - Borrower record
    */
    public static Database.SaveResult insertBorrower(Borrower__c item) {
        return Database.insert(item);
    }

    /**
    * Method for insert Application
    *
    * @author Dmitry Goshko
    *
    * @param Application__c item - Application record
    */
    public static Database.SaveResult insertApplication(Application__c item) {
        return Database.insert(item);
    }
}