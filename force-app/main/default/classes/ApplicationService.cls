/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * ApplicationService class for getting Community data 
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           05/25/2021                   Created method for get data on Refinance Purpose slide
 * Dmitry Goshko           05/25/2021                   Created method for get data on My Current Mortgage slide
 * Dmitry Goshko           05/25/2021                   Created method for get data on Home Info slide
 * Dmitry Goshko           04/24/2021                   Created method for Who's on the Loan slide
 * Dmitry Goshko           04/24/2021                   Created method for My Credit slide
 * Dmitry Goshko           04/25/2021                   Created method for Income slide
 * Dmitry Goshko           04/25/2021                   Created method for add another job on Income slide
 * Dmitry Goshko           04/29/2021                   Created method for Government Questions slide
 * Dmitry Goshko           05/05/2021                   Created method for get Home Info
 * Dmitry Goshko           05/12/2021                   Created method for save Alimony Payment on Government Question slide
 * Dmitry Goshko           05/17/2021                   Created method for get information on Refinance Solution slide
 * Dmitry Goshko           05/25/2021                   Created method for Upload files
**/
public with sharing class ApplicationService {

    /**
    * Method for get get Last Application Stagede
    *
    * @author Dmitry Goshko
    *
    * @param String applicationId - id of Application record
    */
    @Auraenabled
    public static Map<String, Decimal> getLastApplicationStage(String applicationId) {
        Map<String, Decimal> wrapper = new Map<String, Decimal>();

        for(Application__c app : [SELECT Id, Refinance_Purpose__c, Application_Stage__c, Last_Application_Stage__c FROM Application__c WHERE Id =: applicationId]) {
            if(app.Last_Application_Stage__c != null) {
                wrapper.put('lastApplicationStage', app.Last_Application_Stage__c);
            } else {
                wrapper.put('lastApplicationStage', 0);
            }
            
        }

        return wrapper;
    }

    /**
    * Method for get data on Refinance Purpose slide
    *
    * @author Dmitry Goshko
    *
    * @param String applicationId - id of Application record
    */
    @Auraenabled
    public static Map<String, String> getRefinancePurpose(String applicationId) {
        Map<String, String> wrapper = new Map<String, String>();

        for(Application__c app : [SELECT Id, Refinance_Purpose__c, Application_Stage__c, Last_Application_Stage__c FROM Application__c WHERE Id =: applicationId]) {
            wrapper.put('refinancePurpose', app.Refinance_Purpose__c);
            wrapper.put('applicationId', app.Id);
            wrapper.put('applicationStage', app.Application_Stage__c);
            wrapper.put('lastApplicationStage', String.valueOf(app.Last_Application_Stage__c));
        }

        return wrapper;
    }

    /**
    * Method for get data on My Current Mortgage slide
    *
    * @author Dmitry Goshko
    *
    * @param String applicationId - id of Application record
    */
    @Auraenabled
    public static Map<String, String> getMyCurrentMortgage(String applicationId) {
        Map<String, String> wrapper = new Map<String, String>();

        for(Application__c app : [SELECT Id, Refinance_Purpose__c, Application_Stage__c, Last_Application_Stage__c FROM Application__c WHERE Id =: applicationId]) {
            wrapper.put('applicationStage', app.Application_Stage__c);
            wrapper.put('applicationId', app.Id);
            wrapper.put('lastApplicationStage', String.valueOf(app.Last_Application_Stage__c));
        }

        for(Borrower__c bor : [SELECT Id, Application__c, Military_or_Surviving_Spouse__c FROM Borrower__c WHERE Application__c =: applicationId]) {
            wrapper.put('borrowerId', bor.Id);
            wrapper.put('militarySurvivingSpouse', bor.Military_or_Surviving_Spouse__c);
        }

        for(REO__c reo : [SELECT Id, Application__c, Mortgage_Payment__c, Pmt_Includes_P_I__c, Mortgage_Lien__c, Market_Value__c 
                          FROM REO__c WHERE Application__c =: applicationId]) {
            wrapper.put('mortgagePayment', String.valueOf(reo.Mortgage_Payment__c));
            wrapper.put('pmtIncludesPI', String.valueOf(reo.Pmt_Includes_P_I__c));
            wrapper.put('mortgageLien', String.valueOf(reo.Mortgage_Lien__c));
            wrapper.put('marketValue', String.valueOf(reo.Market_Value__c));
        }

        return wrapper;
    }

    /**
    * Method for get data on My Home Info slide
    *
    * @author Dmitry Goshko
    *
    * @param String applicationId - id of Application record
    */
    @Auraenabled
    public static Map<String, String> getHomeInfoFirstPart(String applicationId) {
        Map<String, String> wrapper = new Map<String, String>();

        for(Application__c app : [SELECT Id, Refinance_Purpose__c, Application_Stage__c, Last_Application_Stage__c FROM Application__c WHERE Id =: applicationId]) {
            wrapper.put('applicationStage', app.Application_Stage__c);
            wrapper.put('applicationId', app.Id);
            wrapper.put('lastApplicationStage', String.valueOf(app.Last_Application_Stage__c));
        }

        for(REO__c reo : [SELECT Id, Postal_Code__c, State__c, City__c, Address_2__c, Address__c FROM REO__c WHERE Application__c =: applicationId]) {
            wrapper.put('address', reo.Address__c);
            wrapper.put('address2', reo.Address_2__c);
            wrapper.put('city', reo.City__c);
            wrapper.put('state', reo.State__c);
            wrapper.put('postalCode', reo.Postal_Code__c);
            wrapper.put('reoId', reo.Id);
        }

        return wrapper;
    }

    /**
    * Method for get data on My Home Info slide
    *
    * @author Dmitry Goshko
    *
    * @param String applicationId - id of Application record
    */
    @Auraenabled
    public static Map<String, String> getHomeInfoSecondPart(String applicationId) {
        Map<String, String> wrapper = new Map<String, String>();

        for(Application__c app : [SELECT Id, Refinance_Purpose__c, Application_Stage__c, Last_Application_Stage__c FROM Application__c WHERE Id =: applicationId]) {
            wrapper.put('applicationStage', app.Application_Stage__c);
            wrapper.put('applicationId', app.Id);
            wrapper.put('lastApplicationStage', String.valueOf(app.Last_Application_Stage__c));
        }

        for(REO__c reo : [SELECT Id, Property_Type__c, Occupancy__c, Operate_Business_on_Property__c, Tax_Expense__c, Insurance_Provider__c,
                          Insurance_Expense__c, Original_Purchase_Prices__c, Year_Built__c, Started_Living_Here_Month__c, Started_Living_Here_Year__c 
                          FROM REO__c WHERE Application__c =: applicationId]) {
            wrapper.put('reoId', reo.Id);
            wrapper.put('oropertyType', reo.Property_Type__c);
            wrapper.put('occupancy', reo.Occupancy__c);
            wrapper.put('operateBusinessOnProperty', String.valueOf(reo.Operate_Business_on_Property__c));
            wrapper.put('taxExpense', String.valueOf(reo.Tax_Expense__c));
            wrapper.put('insuranceProvider', String.valueOf(reo.Insurance_Provider__c));
            wrapper.put('insuranceExpense', String.valueOf(reo.Insurance_Expense__c));
            wrapper.put('originalPurchasePrices', String.valueOf(reo.Original_Purchase_Prices__c));
            wrapper.put('yearBuilt', String.valueOf(reo.Year_Built__c));
            wrapper.put('startedLivingHereMonth', String.valueOf(reo.Started_Living_Here_Month__c));
            wrapper.put('startedLivingHereYear', String.valueOf(reo.Started_Living_Here_Year__c));
        }

        return wrapper;
    }

    /**
    * Method for get data on Who's on the Loan slide
    *
    * @author Dmitry Goshko
    *
    * @param String applicationId - id of Application record
    */
    @Auraenabled
    public static Map<String, String> getWhosOnTheLoan(String applicationId) {
        Map<String, String> wrapper = new Map<String, String>();

        for(Application__c app : [SELECT Id, Refinance_Purpose__c, Application_Stage__c, Last_Application_Stage__c FROM Application__c WHERE Id =: applicationId]) {
            wrapper.put('applicationStage', app.Application_Stage__c);
            wrapper.put('applicationId', app.Id);
            wrapper.put('lastApplicationStage', String.valueOf(app.Last_Application_Stage__c));
        }

        for(Borrower__c bor : [SELECT Id, Marital_Status__c, First_Name__c, Type__c, Last_Name__c, Email__c, Spouse_Military_or_Surviving_Spouse__c 
                               FROM Borrower__c WHERE Application__c =: applicationId]) {
            wrapper.put('borrowerId', bor.Id);
            wrapper.put('maritalStatus', bor.Marital_Status__c);
            wrapper.put('firstName', bor.First_Name__c);
            wrapper.put('type', bor.Type__c);
            wrapper.put('lastName', bor.Last_Name__c);
            wrapper.put('email', bor.Email__c);
            wrapper.put('militaryOrSurviving', bor.Spouse_Military_or_Surviving_Spouse__c);
        }

        return wrapper;
    }
}