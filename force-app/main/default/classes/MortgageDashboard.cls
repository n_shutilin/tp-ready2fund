/**
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
 * MortgageDashboard class for work with Applications on Dashboard.
 * ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 * Developer               Modified Date    Jira Id     Description
 * ================================================================================================================================
 * Dmitry Goshko           04/30/2021                   Created method for deleteMyApplication
 * Dmitry Goshko           04/30/2021                   Created method for startANewApplication
**/
global with sharing class MortgageDashboard {

    /**
    * Method for delete Application on Mortgage Dashboard
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    global static Boolean deleteMyApplication(Map<String, String> wrapper) {
        
        Database.DeleteResult result = Database.delete(wrapper.get('applicationId'));

        return result.success;
    }

    /**
    * Method for create new Application on Mortgage Dashboard
    *
    * @author Dmitry Goshko
    *
    * @param Map<String, String> wrapper - Map with needed values comes from LWC
    */
    @Auraenabled
    global static String startANewApplication(Map<String, String> wrapper) {
        Application__c app = new Application__c();
        Map<String, Ready2Fund_Community_Seetings__mdt> communitySettings = SubmitApplication.getCommunitySettings();

        if(wrapper.containsKey('partnerAccountId')) {
            app.Account__c = wrapper.get('partnerAccountId');
        } else {
            app.Account__c = communitySettings.get('Community_Settings').Partner_Account_Id__c;
        }

        insert app;

        return app.id;
    }
}